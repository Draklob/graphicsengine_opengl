/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include <iostream>
#include <memory>

#include "Application.h"

#undef main

using namespace mutant;

int main(int number, char** argv) {

	// We create a application of the graphic engine.
	std::shared_ptr<Application> app(new Application() );
	app->launch( argv[1]);
//	app->launch();

	return 0;
}