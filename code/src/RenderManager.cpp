/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Render Manager												*
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

// This class "RenderManager" handles all the settings of the rendering process.

#include <stdlib.h>		// atexit
#include <assert.h>		// assert
#include <iostream>		// cout & cin

#include "RenderManager.h"

namespace mutant
{
	RenderManager::RenderManager()
	:
		win( new RenderWindow())
	{
	}

	RenderManager::~RenderManager()
	{

	}

	RenderManager& RenderManager::instance()
	{
		static RenderManager *pInstance;

		if (pInstance == NULL)
			pInstance = new RenderManager;

		assert(pInstance);

		return *pInstance;

	}

	void RenderManager::startUp()
	{
		std::cout << "Arracando Render Manager." << std::endl;
		
		//RenderWindow *rw = new RenderWindow();
		// Esto est� comentado, pero hay que descomentarlo, se encarga de renderizar la ventana.
		while ( win->render() );
	}

	void RenderManager::shutDown()
	{
		std::cout << "Cerrando Render Manager." << std::endl;
	}

	void RenderManager::DestroyInstance()
	{
		
	}

	shared_ptr<RenderWindow> RenderManager::getRenderWindow()
	{
		return win;
	}

}