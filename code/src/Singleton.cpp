/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Singleton.h"

#include <iostream>		// cout & cin
#include <stdlib.h>		// atexit

namespace mutant
{
	// Initialize the pointer pInstancia.
	Singleton* Singleton::pInstance = NULL;

	Singleton& Singleton::getInstance()
	{
		if (pInstance == NULL)
		{
			// Create the instance.
			pInstance = new Singleton();
			atexit( &DestroySingleton);
		}

		return *pInstance;
	}

	void Singleton::DestroySingleton()
	{
		if ( pInstance != NULL )
			delete pInstance;
	}

	void Singleton::showMessage() const
	{
		std::cout << "Llamando a Singleton." << std::endl;
	}
}