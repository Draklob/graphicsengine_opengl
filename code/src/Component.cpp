/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Component.h"

namespace mutant
{
	string Component::getComponentID()
	{
		return ID;
	}

	void Component::setGameObject( shared_ptr<GameObject> go )
	{
		gameObject = go;
	}

	shared_ptr<GameObject> Component::getGameObject() const
	{
		return gameObject;
	}
}