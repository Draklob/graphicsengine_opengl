/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Camera.h"

namespace mutant
{
	Camera::Camera(float near_plane, float far_plane, float fov, float width, float height)
	:
		projection(near_plane, far_plane, fov, width / height)
	{

	}

	Camera::~Camera()
	{

	}

	// Sets the projection of the camera.
	void Camera::setProjection(const float& near, const float& far, const float& fov, const float& width, const float& height)
	{
		projection.set( near, far, fov, width / height );
	}

	// Returns the projection of the camera.
	const Projection3f& Camera::getProjection() const
	{
		return projection;
	}
}