/* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * *\
*																				*
*		Javier Barreiro Portela													*
*																				*
*		22 April 2015  - Programmning Graphics									*
*																				*
*		Design & Develop of Videogames											*
*																				*
*		jbarreiro.23@gmail.com													*
*																				*
\* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

#include "Material.h"

#include "ResourceManager.h"

namespace mutant
{
	Material::Material( int id, std::string name )
	:
		id( id ),
		name( name )
	{
		//setup shader
		shader.loadFromFile(GL_VERTEX_SHADER, "..\\..\\..\\shaders\\shader.vert");
		shader.loadFromFile(GL_FRAGMENT_SHADER, "..\\..\\..\\shaders\\shader.frag");
		shader.createAndLinkProgram();
		shader.use();
		// Vertex Shader
		shader.addAttribute("vVertex");
		shader.addAttribute("vNormal");
		shader.addAttribute("vUV");
		shader.addUniform("MVP");
		shader.addUniform("MV");
		shader.addUniform("NM");
		// Fragment Shader
		shader.addUniform("light_position");
		shader.addUniform("diffuse_color");
		shader.addUniform("specular_color");
		shader.addUniform("shininess");
		shader.addUniform("textureMap");
		glUniform1i(shader("textureMap"), 0);
		shader.unUse();
	}

	Material::Material()
	:
		id( 0 ),
		name(" " )
	{/*
		//setup shader
		shader.loadFromFile(GL_VERTEX_SHADER, "..\\..\\..\\shaders\\shader.vert");
		shader.loadFromFile(GL_FRAGMENT_SHADER, "..\\..\\..\\shaders\\shader.frag");
		shader.createAndLinkProgram();
		shader.use();
		shader.addAttribute("vVertex");
		shader.addAttribute("vNormal");
		shader.addAttribute("vUV");
		shader.addUniform("MVP");
		shader.addUniform("textureMap");
		glUniform1i(shader("textureMap"), 0);
		shader.unUse();*/
	}

	// Default material. We need the size to set the color to the material.
	Material::Material( size_t size)
	:
		id( ResourceManager::instance().list_materials.size() ),
		name( "DefaultDiffuse")
	{
	}

	Material::~Material()
	{

	}

	int Material::getID()
	{
		return id;
	}

	std::string Material::getName()
	{
		return name;
	}

	Material* Material::getMaterial()
	{
		return this;
	}

	void Material::setTexture(shared_ptr< Texture > textur)
	{
		texture = textur;
	}

	shared_ptr< Texture > Material::getTexture() const
	{
		return texture;
	}

	void Material::setShader(GLSLShader shad)
	{
		shader = shad;
	}

	GLSLShader Material::getShader() const
	{
		return shader;
	}
}