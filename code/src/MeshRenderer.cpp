/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "MeshRenderer.h"
#include "Material.h"
#include "Mesh.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>


namespace mutant
{
	MeshRenderer::MeshRenderer()
	{

	}

	MeshRenderer::MeshRenderer( Mesh* mes, Material* mat )
		:
		material( mat),
		mesh( mes )
	{
		setupMesh();
	}

	MeshRenderer::~MeshRenderer()
	{

	}

	void MeshRenderer::setMaterial( Material* mat)
	{
		material = mat;
	}

	void MeshRenderer::setMesh(Mesh* mes)
	{
		mesh = mes;
	}

	void MeshRenderer::setupMesh()
	{
		// Create buffers/arrays
		glGenVertexArrays(1, &vaoID);	// vertexArrayObject
		glGenBuffers( NUM_BUFFERS, vAB);	// Num of buffers.
	//	glGenBuffers(1, &vboIndicesID);

		//get total vertices and indices
		totalVertices = mesh->getVertices().size();
		totalIndices = mesh->getIndices().size();
		
		//now allocate buffers
		glBindVertexArray(vaoID);

		// Vertices
		glBindBuffer(GL_ARRAY_BUFFER, vAB[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, totalVertices * sizeof(GLfloat), &mesh->getVertices()[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(POSITION_VB);
		glVertexAttribPointer(POSITION_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

		// Normals
		glBindBuffer(GL_ARRAY_BUFFER, vAB[NORMAL_VB]);
		glBufferData(GL_ARRAY_BUFFER, mesh->getNormals().size() * sizeof(GLfloat), &mesh->getNormals()[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(NORMAL_VB);
		glVertexAttribPointer(NORMAL_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

		// UVs
		glBindBuffer(GL_ARRAY_BUFFER, vAB[TEXCOORD_VB]);
		glBufferData(GL_ARRAY_BUFFER, mesh->getUVs().size() * sizeof(GLfloat), &mesh->getUVs()[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(TEXCOORD_VB);
		glVertexAttribPointer(TEXCOORD_VB, 2, GL_FLOAT, GL_FALSE, 0, 0);

		// Indices
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vAB[INDEX_VB]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalIndices * sizeof(GLushort), &mesh->indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}

	void MeshRenderer::render(const GLfloat* MVP, const GLfloat* MV)
	{
		glm::vec3 lightPosOS = glm::vec3(0.0f, 0.0f, 0.0f);

		material->getShader().use();
			glUniformMatrix4fv(material->getShader()("MVP"), 1, GL_FALSE, MVP );
			glUniformMatrix4fv(material->getShader()("MV"), 1, GL_FALSE, MV);
			//glUniformMatrix3fv(material->getShader()("N"), 1, GL_FALSE, glm::value_ptr(glm::inverseTranspose(glm::mat3(MV))));
			glUniform3f(material->getShader()("diffuse_color"),0.0f, 0.0f, 1.0f);
			glUniform3f(material->getShader()("specular_color"), 1.0f, 1.0f, 1.0f);
			glUniform1f(material->getShader()("shininess"), 100);
			glUniform3f(material->getShader()("light_position"), 0.0f, 0.0f, 0.0f);
			glBindVertexArray( vaoID );
			glDrawElements(GL_TRIANGLES, totalIndices * sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
		material->getShader().unUse();
	}
}