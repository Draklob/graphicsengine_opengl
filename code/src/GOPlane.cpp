#include "GOPlane.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Scenemanager.h"

namespace mutant
{
	GOPlane::GOPlane(const int w, const int d)
	{
		width = w;
		depth = d;

		//	shad.loadShaderFromFile("..\\..\\shaders\\checker_shader.vert", GL_VERTEX_SHADER);

		//setup shader
		shader.loadFromFile(GL_VERTEX_SHADER, "..\\..\\..\\shaders\\shader.vert");
		shader.loadFromFile(GL_FRAGMENT_SHADER, "..\\..\\..\\shaders\\shader.frag");
		shader.createAndLinkProgram();
		shader.use();
		shader.addAttribute("vVertex");
		shader.addAttribute("vNormal");
		shader.addAttribute("vUV");
		shader.addUniform("MVP");
		shader.addUniform("textureMap");
		glUniform1i(shader("textureMap"), 0);
		shader.unUse();

		Init();
	}

	void GOPlane::draw()
	{
		glm::mat4 P, T, MV, MVP;
		P = SceneManager::instance().getScene()->getCamera()->GetProjectionMatrix();
		MV = SceneManager::instance().getScene()->getCamera()->GetViewMatrix();
		T = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
		MVP = P*T*MV;

		//render the chekered plane
		Render(glm::value_ptr(MVP));
	}

	shared_ptr<GOPlane> GOPlane::createPlane(int width, int height)
	{
		//floor checker texture ID
		GLuint checkerTextureID;

		//generate the checker texture
		GLubyte data[128][128] = { 0 };
		for (int j = 0; j<128; j++) {
			for (int i = 0; i<128; i++) {
				data[i][j] = (i <= 64 && j <= 64 || i>64 && j>64) ? 255 : 0;
			}
		}

		shared_ptr<GOPlane> plane = make_shared<GOPlane>(width, height);

		////generate texture object
		//glGenTextures(1, &checkerTextureID);
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, checkerTextureID);
		////set texture parameters
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		////set maximum aniostropy setting
		//// Este parrafo no hace nada en este caso o eso creo.
		//GLfloat largest_supported_anisotropy;
		//glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy);
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);

		////set mipmap base and max level
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);

		////allocate texture object
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, 128, 128, 0, GL_RED, GL_UNSIGNED_BYTE, data);

		////generate mipmaps
		//glGenerateMipmap(GL_TEXTURE_2D);

		return plane;
	}


	GOPlane::~GOPlane(void)
	{
	}

	int GOPlane::GetTotalVertices() {
		return 4;
	}

	int GOPlane::GetTotalIndices() {
		return 6;
	}

	GLenum GOPlane::GetPrimitiveType() {
		return GL_TRIANGLES;
	}

	void GOPlane::FillVertexBuffer(GLfloat* pBuffer) {
		glm::vec3* vertices = (glm::vec3*)(pBuffer);

		int width_2 = width / 2;
		int depth_2 = depth / 2;

		vertices[0] = glm::vec3(-width_2, 0, -depth_2);
		vertices[1] = glm::vec3(width_2, 0, -depth_2);

		vertices[2] = glm::vec3(width_2, 0, depth_2);
		vertices[3] = glm::vec3(-width_2, 0, depth_2);
	}

	void GOPlane::FillIndexBuffer(GLuint* pBuffer) {

		//fill indices array
		GLuint* id = pBuffer;
		*id++ = 0;
		*id++ = 1;
		*id++ = 2;
		*id++ = 0;
		*id++ = 2;
		*id++ = 3;
	}
}