#include "Box.h"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Scenemanager.h"

namespace mutant
{
	Box::Box()
	{
		//setup shader
		shader.loadFromFile(GL_VERTEX_SHADER, "..\\..\\..\\shaders\\shader.vert");
		shader.loadFromFile(GL_FRAGMENT_SHADER, "..\\..\\..\\shaders\\shader.frag");
		shader.createAndLinkProgram();
		shader.use();
		shader.addAttribute("vVertex");
		shader.addAttribute("vNormal");
		shader.addAttribute("vUV");
		shader.addUniform("MVP");
		shader.addUniform("textureMap");
		glUniform1i(shader("textureMap"), 0);
		shader.unUse();

		Init();
	}

	void Box::draw()
	{
		glm::mat4 P, T, MV, MVP;
		P = SceneManager::instance().getScene()->getCamera()->GetProjectionMatrix();
		MV = SceneManager::instance().getScene()->getCamera()->GetViewMatrix();
		T = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
		MVP = P*T*MV;

		//render the chekered plane
		Render(glm::value_ptr(MVP));
	}

	void Box::create()
	{
		//floor checker texture ID
		GLuint boxTextureID;

		//generate texture object
		glGenTextures(1, &boxTextureID);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, boxTextureID);
		//set texture parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		//set maximum aniostropy setting
		// Este parrafo no hace nada en este caso o eso creo.
		GLfloat largest_supported_anisotropy;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);

		//set mipmap base and max level
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);

		//allocate texture object
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, 128, 128, 0, GL_RED, GL_UNSIGNED_BYTE, data);

		//generate mipmaps
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	Box::~Box(void)
	{
	}

}