/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include <assert.h>
#include <iostream>

#include <obj_loader.hpp>
#include <vbo_indexer.hpp>


#include "ResourceManager.h"

namespace mutant
{
	using namespace std;

	ResourceManager::ResourceManager()
	{
		counterObjects = 0;
	}

	ResourceManager::~ResourceManager()
	{

	}
	
	ResourceManager& ResourceManager::instance()
	{
		static ResourceManager *pInstance;

		if (pInstance == NULL)
			pInstance = new ResourceManager();

		assert(pInstance);

		return *pInstance;
	}

	// Load a .obj with your materials and meshes with the library TinyObjLoader.
	void ResourceManager::loadObjects( std::string inputFile )
	{
		shapes.clear();
		materials.clear();

		std::string err = tinyobj::LoadObj(shapes, materials, inputFile.c_str(), "../../../assets/");
		if (!err.empty()) {
			std::cerr << err << std::endl;
			exit(1);
		}

		std::string nameMesh = inputFile;
		nameMesh.erase(nameMesh.begin(), nameMesh.begin() + 16);
		nameMesh.erase(nameMesh.end() - 4, nameMesh.end());

		std::cout << "# of shapes    : " << shapes.size() << std::endl;
		std::cout << "# of materials : " << materials.size() << std::endl;
		std::cout << "" << std::endl;

		int numObjects = shapes.size();

		// Object's mesh.
		shared_ptr<Mesh> mesh = make_shared<Mesh>(list_meshes.size(), nameMesh);
		// Object's material.
		shared_ptr< Material > material = make_shared< Material >(list_materials.size(), (nameMesh + "Mat"));

		// We travel every object.
		// This is planned taking into account that the objects have a single material.
		// No multi-material.
		for ( int obj = 0, mat = 0; obj < numObjects; obj++, mat++ )
		{
			tinyobj::mesh_t meshTiny = shapes[obj].mesh;

			

			cout << meshTiny.indices.size() << endl;
			cout << meshTiny.normals.size()  << endl;
			cout << meshTiny.positions.size()  << endl;
			cout << meshTiny.texcoords.size() << endl;

			std::vector<GLfloat> temp_vertices;
			std::vector<GLfloat> temp_uvs;
			std::vector<GLfloat> temp_normals;
			std::vector<GLushort> temp_indices;

			temp_vertices = meshTiny.positions;
			temp_normals = meshTiny.normals;
			temp_uvs = meshTiny.texcoords;
			for (int i = 0; i < meshTiny.indices.size(); i++)
			{

				temp_indices.push_back(meshTiny.indices[i]);

			}
			mesh->setVertices(temp_vertices);
			mesh->setNormals(temp_normals);
			mesh->setUVs(temp_uvs);
			mesh->setIndices(temp_indices);

			/*
			cout << meshTiny.indices.size() << endl;
			cout << meshTiny.normals.size() / 3 << endl;
			cout << meshTiny.positions.size() / 3 << endl;
			cout << meshTiny.texcoords.size() / 2 << endl;

			std::vector<glm::vec3> temp_vertices;
			std::vector<glm::vec2> temp_uvs;
			std::vector<glm::vec3> temp_normals;
			std::vector<GLushort> temp_indices;

			size_t number_of_v = (meshTiny.positions.size() / 3);
			temp_vertices.resize(number_of_v);

			for (size_t v = 0; v < number_of_v; v++)
			{
				temp_vertices[v][0] = meshTiny.positions[3 * v + 0];
				temp_vertices[v][1] = meshTiny.positions[3 * v + 1];
				temp_vertices[v][2] = meshTiny.positions[3 * v + 2];

			}
			mesh->setVertices(temp_vertices);

			temp_normals.resize(number_of_v);
			for (size_t v = 0; v < number_of_v; v++)
			{
				temp_normals[v][0] = meshTiny.normals[3 * v + 0];
				temp_normals[v][1] = meshTiny.normals[3 * v + 1];
				temp_normals[v][2] = meshTiny.normals[3 * v + 2];
			}
			mesh->setNormals(temp_normals);

			// Setting UVs
			size_t number_of_uv = meshTiny.texcoords.size() / 2;

			temp_uvs.resize(number_of_uv);
			for (size_t v = 0; v < number_of_uv; v++)
			{
				temp_uvs[v][0] = meshTiny.texcoords[2 * v + 0];
				temp_uvs[v][1] = meshTiny.texcoords[2 * v + 1];
			}
			mesh->setUVs(temp_uvs);

			// Setting Indices
			size_t number_of_idx = meshTiny.indices.size();

			temp_indices.resize(number_of_idx);
			for (size_t idx = 0, id = 0; id < number_of_idx; idx++)
			{
				temp_indices[id++] = meshTiny.indices[3 * idx + 0];
				temp_indices[id++] = meshTiny.indices[3 * idx + 1];
				temp_indices[id++] = meshTiny.indices[3 * idx + 2];
			}
			mesh->setIndices(temp_indices);
			*/
		}
		list_meshes.push_back(mesh);
		list_materials.push_back(material);

	}

	// Load a object for the mesh ( Vertices, Indices, Normals & UVs ).
	void ResourceManager::loadObject(const char * pathObj)
	{/*
		std::string nameMesh = pathObj;
		nameMesh.erase(nameMesh.begin(), nameMesh.begin() + 16);
		nameMesh.erase(nameMesh.end() - 4, nameMesh.end());

		// Object's mesh.
		shared_ptr<Mesh> mesh = make_shared<Mesh>(list_meshes.size(), nameMesh);
		shared_ptr< Material > material = make_shared< Material >(list_materials.size(), ( nameMesh + "Mat" ) ) ;

		std::vector<glm::vec3> temp_vertices;
		std::vector<glm::vec2> temp_uvs;
		std::vector<glm::vec3> temp_normals;
		std::vector<GLushort> temp_indices;


		if (pathObj != "" && loadOBJ(pathObj, temp_vertices, temp_uvs, temp_normals))
		{
			std::vector<glm::vec3> indexed_vertices;
			std::vector<glm::vec2> indexed_uvs;
			std::vector<glm::vec3> indexed_normals;

			indexVBO(temp_vertices, temp_uvs, temp_normals, temp_indices, indexed_vertices, indexed_uvs, indexed_normals);

			
			mesh->setIndices( temp_indices );
			mesh->setVertices( indexed_vertices );
			mesh->setUVs(indexed_uvs);
			mesh->setNormals(indexed_normals);

			std::cout << "Indices: " << temp_indices.size() << ", Vertices: " << indexed_vertices.size() << ", Normals: " << indexed_normals.size() << ", UVs: " << indexed_uvs.size() << std::endl;
		}

		list_meshes.push_back( mesh );
		list_materials.push_back( material );*/
	}

	void ResourceManager::loadTexture( const string inputFile )
	{
		std::string nameTexture = inputFile;
		nameTexture.erase(nameTexture.begin(), nameTexture.begin() + 14);
		nameTexture.erase(nameTexture.end() - 4, nameTexture.end() );
		// Texture																					 We erase the last 4 characters ".png"
		shared_ptr< Texture > texture = make_shared< Texture >(list_textures.size(), nameTexture);

		SDL_Surface* mTexture = NULL;

		mTexture = IMG_Load(inputFile.c_str());
		if (!mTexture) {
			// handle error
			std::cout << "unable to load texture " << inputFile << "\n";
		}

		SDL_PixelFormat *fmt;
		fmt = mTexture->format;
		GLuint format = GL_RGB;
		switch (fmt->format)
		{
		case SDL_PIXELFORMAT_RGB24:
			format = GL_RGB;
			break;
		case SDL_PIXELFORMAT_BGR24:
			format = GL_BGR;
			break;
		case SDL_PIXELFORMAT_ABGR8888:
			format = GL_RGBA;
			break;
		case SDL_PIXELFORMAT_ARGB8888:
			format = GL_BGRA;
			break;

		default:
			std::cout << "SDL_PIXELFORMAT_RGB444" << " " << SDL_PIXELFORMAT_RGB444 << "\n";
			std::cout << "SDL_PIXELFORMAT_RGB555" << " " << SDL_PIXELFORMAT_RGB555 << "\n";
			std::cout << "SDL_PIXELFORMAT_RGB565" << " " << SDL_PIXELFORMAT_RGB565 << "\n";
			std::cout << "SDL_PIXELFORMAT_RGB888" << " " << SDL_PIXELFORMAT_RGB888 << "\n";
			std::cout << "SDL_PIXELFORMAT_RGBA8888" << " " << SDL_PIXELFORMAT_RGBA8888 << "\n";
		}
		
		GLuint mtexture;
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1, &mtexture);   //Creates a texture
		glBindTexture(GL_TEXTURE_2D, mtexture);    //binds the texture
		
		//https://www.opengl.org/sdk/docs/man/html/glTexParameter.xhtml
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//tells it to repeat texture if it is beyond the dimensional size
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);//linearly interpolate if texture needs to be reduced
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//linearly interpolate if texture needs to be magnified
		
		//loads the data and tells
		//https://www.opengl.org/sdk/docs/man3/xhtml/glTexImage2D.xml
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mTexture->w, mTexture->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mTexture->pixels);

		texture->setTexture( mtexture );
		texture->setSurface( mTexture );

		list_textures.push_back( texture );
	}

	// We create the meshes of the objet/s.
	void ResourceManager::createMesh(tinyobj::mesh_t meshObject, int idObject)
	{/*
		// Create the mesh of the object.
		shared_ptr< Mesh> mesh = make_shared< Mesh >( ( list_meshes.size() + 1), shapes[idObject].name.c_str( ) );
		
		// Setting Vertices
		assert((meshObject.positions.size() % 3) == 0);
		size_t number_of_v = (meshObject.positions.size() / 3);

		mesh->original_vertices.resize(number_of_v);
		mesh->transformed_vertices.resize( number_of_v );
		mesh->display_vertices.resize( number_of_v );

		for (size_t v = 0; v < number_of_v; v++) 
		{
			mesh->original_vertices[v][0] = meshObject.positions[3 * v + 0];
			mesh->original_vertices[v][1] = meshObject.positions[3 * v + 1];
			mesh->original_vertices[v][2] = meshObject.positions[3 * v + 2];
			mesh->original_vertices[v][3] = 1;
		}

		// Setting Normals
		size_t number_of_vn = meshObject.normals.size() / 3;
		
		mesh->normals_buffer.resize( number_of_vn );
		for (size_t v = 0; v < number_of_vn; v++)
		{
			mesh->normals_buffer[v][0] = meshObject.normals[3 * v + 0];
			mesh->normals_buffer[v][1] = meshObject.normals[3 * v + 1];
			mesh->normals_buffer[v][2] = meshObject.normals[3 * v + 2];
		}

		// Setings UVs
		size_t number_of_UVs = meshObject.texcoords.size() / 3;

		// Setting Indices
		size_t number_of_idx = meshObject.indices.size();

		mesh->original_indices.resize( number_of_idx );
		for (size_t idx = 0, id = 0; id < number_of_idx; idx++)
		{
			mesh->original_indices[id++] = meshObject.indices[3 * idx + 0];
			mesh->original_indices[id++] = meshObject.indices[3 * idx + 1];
			mesh->original_indices[id++] = meshObject.indices[3 * idx + 2];
		}

		// We store the mesh of the object in the list with all your data.
		std::cout << "Creando malla para " << shapes[idObject].name.c_str() << " con " << number_of_v << " vertices, " << number_of_vn
				  << " normales, " << number_of_UVs << " texcoords, " << number_of_idx << " indices." << std::endl;
		list_meshes.push_back( mesh );
		*/
	}

	// We create the material/s.
	void ResourceManager::createMaterial(int material, int obj)
	{
		// We create the material to store it.
		std::shared_ptr< Material > mat = make_shared< Material> ( ( list_materials.size() + 1 ), shapes[obj].name.c_str() );
		/*
		mat->original_colors.resize( shapes[obj].mesh.indices.size() / 3 );

		for (size_t idx = 0; idx < mat->original_colors.size(); idx++)
		{
			// Get the colors RGB of every face of the triangle.
			int r,g,b;
			int mat_id = shapes[obj].mesh.material_ids[idx];
			r = int( materials[mat_id].diffuse[0] * 255 );
			g = int( materials[mat_id].diffuse[1] * 255 );
			b = int( materials[mat_id].diffuse[2] * 255 );

			mat->original_colors[idx].set( r, g, b );
		}
		*/
		// We store the material of the object in the list with all your data.
		std::cout << "Creando material -> \" " << materials[material].name.c_str() << " \" para el objeto " << shapes[obj].name.c_str() << std::endl;
		list_materials.push_back(mat);
	}
}