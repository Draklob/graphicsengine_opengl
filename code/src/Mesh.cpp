/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Mesh.h"

namespace mutant
{
	Mesh::Mesh(int id, std::string name)
	:
		id( id ),
		name( name )
	{

	}
	
	Mesh::Mesh()
	:
		id( 0 ),
		name(" ")
	{
		
	}

	Mesh::~Mesh()
	{

	}

	std::string Mesh::getName()
	{
		return name;
	}

	int Mesh::getID()
	{
		return id;
	}

	// We set the vertices of the mesh.
	void Mesh::setVertices(std::vector<GLfloat> verts)
	{
		position = verts;
	}

	// We get the vertices of the mesh.
	std::vector<GLfloat> Mesh::getVertices()
	{
		return position;
	}

	// We set the UVs of the mesh.
	void Mesh::setUVs(std::vector<GLfloat> uvs)
	{
		UVs = uvs;
	}

	// We get the UVs of the mesh.
	std::vector<GLfloat> Mesh::getUVs()
	{
		return UVs;
	}

	// We set the normals of the mesh.
	void Mesh::setNormals(std::vector<GLfloat> norms)
	{
		normals = norms;
	}

	// We get the normals of the mesh.
	std::vector<GLfloat> Mesh::getNormals()
	{
		return normals;
	}

	// We set the indices of the mesh.
	void Mesh::setIndices(std::vector<GLushort> inds)
	{
		indices = inds;
	}

	// We get the indices of the mesh.
	std::vector<GLushort> Mesh::getIndices()
	{
		return indices;
	}

	// We get one mesh of the original.
	Mesh* Mesh::getMesh()
	{
		return this;
	}
}