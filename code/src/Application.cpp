

#include "Application.h"
#include <memory>

namespace mutant
{
	Application::Application()
	{
		//Start up SDL and create window
		if (!init())
		{
			printf("Failed to initialize!\n");
		}
		else
		{
			printf("Sucess to initialize!\n");
		}
	}


	Application::~Application()
	{
		SDL_GL_DeleteContext( context );
	}

	// Method that initialize the application.
	void Application::launch(std::string scene)
//	void Application::launch( )
	{		
		// We load the single object of the la scene.
	//	recM->instance().loadObject("../../../assets/box.obj");
		recM->instance().loadObjects("../../../assets/box.obj");
	//	recM->instance().loadObjects("../../../assets/box.obj");
		recM->instance().loadTexture("../../../media/Caja.png");

		// It creates the scene.
	//	scenM->instance().createScene("");
		scenM->instance().createScene(scene);
		
		// It initializes the RenderManager with yours methods.
		renM->instance().startUp();
	}

	bool Application::init()
	{
		// Success flag
		bool success = true;

		//Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Set texture filtering to linear
			if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
			{
				printf("Warning: Linear texture filtering not enabled!");
			}

			//Use OpenGL 3.1 core Parece que = no se necesita
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
			// We check if the window was created success.
			if (renM->instance().getRenderWindow()->getWindow() == NULL)
			{
				printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				// We check if the renderer was created success.
				if (renM->instance().getRenderWindow()->getRenderer() == NULL)
				{
					printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
					success = false;
				}
				else
				{
					//Create context	// It needs for the shaders.
					context = SDL_GL_CreateContext(renM->instance().getRenderWindow()->getWindow());
					if (context == NULL)
					{
						printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
						success = false;
					}
					else
					{
						//Initialize GLEW
						glewExperimental = GL_TRUE;
						GLenum glewError = glewInit();
						if (glewError != GLEW_OK)
						{
							printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
						}

						//Use Vsync
						if (SDL_GL_SetSwapInterval(1) < 0)
						{
							printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
						}
					
						//Initialize renderer color
						SDL_SetRenderDrawColor( renM->instance().getRenderWindow()->getRenderer() , 0xFF, 0xFF, 0xFF, 0xFF);

						//Initialize PNG loading
						int imgFlags = IMG_INIT_PNG;
						if (!(IMG_Init(imgFlags) & imgFlags))
						{
							printf("SDL_image could not be initialized! SDL_image Error: %s\n", IMG_GetError());
							return false;
						}
					}
				}
			}
		}
		return success;
	}
}