/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Render Window												*
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "RenderWindow.h"

#include "SDL_opengl.h"

namespace mutant
{
	RenderWindow::RenderWindow()
	:
		window_width(800),
		window_height(600),
		window( SDL_CreateWindow( "OpenGL Javier Barreiro Portela", 500, 250, window_width, window_height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN ) ),
		renderer( SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED ) ),
		scene( SceneManager::instance().getScene() )
	{
		dt = 0, lastFrameTime = 0, currentFrameTime = 0;
		initialize();
	};

	RenderWindow::RenderWindow(const size_t width, const size_t height)
	:
		window_width(width),
		window_height(height),
		window(SDL_CreateWindow("OpenGL Javier Barreiro Portela", 500, 250, window_width, window_height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN)),
		renderer(SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED)),
		scene(SceneManager::instance().getScene())
	{
		dt = 0, lastFrameTime = 0, currentFrameTime = 0;
		initialize();
	};

	RenderWindow::~RenderWindow()
	{
		SDL_DestroyWindow( window );
		window = NULL;

		SDL_DestroyRenderer( renderer );
		renderer = NULL;
	};

	void RenderWindow::initialize()
	{
		
		
	};
	
	bool RenderWindow::render()
	{
		//timing related calcualtion
		lastFrameTime = currentFrameTime;
		currentFrameTime = (float)SDL_GetTicks();
		dt = (float)(currentFrameTime - lastFrameTime) / 1000;

		// Attend the window events:
			SDL_Event event;

			while (SDL_PollEvent(&event) > 0)
			{
				// If close the app.
				if (event.type == SDL_QUIT)
				{
				//	OnShutdown();
					return false;
				}

				// If we move the mouse.
				if (event.type == SDL_MOUSEMOTION)
				{
					int x, y;
					int button = event.button.button;
					SDL_GetMouseState(&x, &y);

					scene->getCamera()->OnMouseMove(button, x, y);
				}

				// If we click some button of the mouse.
				if (event.type == SDL_MOUSEBUTTONDOWN)
				{
					int button = event.button.button;
					int x, y;
					SDL_GetMouseState(&x, &y);
					scene->getCamera()->OnMouseDown(button, x, y);
				}

				// If we press some key of the keyboard.
				if (event.type == SDL_KEYDOWN)
				{
					Uint16 key = SDL_GetKeyFromScancode(event.key.keysym.scancode);
					scene->getCamera()->moveCamera(key, dt);
				}
			}

		//	SDL_RenderClear(renderer);
		//	SDL_RenderCopy(renderer, NULL, NULL, NULL);
		//	SDL_RenderPresent(renderer);
			glClearColor(1.0, 0.0, 0.0, 1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			//	OnRender(window);

			//Clear screen
			
			scene->update();
			scene->draw();
			
			// Se actualiza el contenido de la ventana:
			SDL_GL_SwapWindow(window);

			
			
		return true;
	};

	void RenderWindow::setResolution( const size_t width, const size_t height)
	{
		window_width = width;
		window_height = height;
	}

	vector<size_t> RenderWindow::getResolution()
	{

		vector<size_t> resolution(2);
		glm::vec2 res;

//		res[0] = window_width;
	//	res[1] = window_height;
		resolution[0] = window_width;
		resolution[1] = window_height;

		return resolution;
		
	}

	SDL_Window* RenderWindow::getWindow()
	{
		return window;
	}

	SDL_Renderer* RenderWindow::getRenderer()
	{
		return renderer;
	}

}