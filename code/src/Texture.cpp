/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Texture.h"

namespace mutant
{
	Texture::Texture(const int id, const std::string name )
	:
		id( id ),
		name( name )
	{

	}

	Texture::~Texture()
	{
		SDL_FreeSurface( m_Texture );
		glDeleteTextures(1, &texture );
	}

	std::string Texture::getName() const
	{
		return name;
	}

	int Texture::getID() const
	{
		return id;
	}

	void Texture::setSurface(SDL_Surface* surface)
	{
		m_Texture = surface;
	}

	SDL_Surface* Texture::getSurface() const
	{
		return m_Texture;
	}

	void Texture::setTexture(GLuint textur)
	{
		texture = textur;
	}

	GLuint Texture::getTexture() const
	{
		return texture;
	}
}