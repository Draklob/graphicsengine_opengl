﻿/* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * *\
*																				*
*		Javier Barreiro Portela													*
*																				*
*		22 April 2015  - Programmning Graphics									*
*																				*
*		Design & Develop of Videogames											*
*																				*
*		jbarreiro.23@gmail.com													*
*																				*
\* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

#include <cmath>
#include <cassert>
#include "View.hpp"
#include <Vector.hpp>
#include <Scaling.hpp>
#include <Rotation.hpp>
#include <Projection.hpp>
#include <Translation.hpp>

#include "../../lib/tiny_loader/tiny_obj_loader.h"
#include <iostream>

using namespace toolkit;

namespace mutant
{
	View::View( size_t width, size_t height )
		:
		width( width ),
		height( height ),
		Color_buffer( width, height ),
		rasterizer( Color_buffer )
	{
					
	}

	void View::update( )
	{
		// Se actualizan los parámetros de transformatión (sólo se modifica el ángulo):

		static float angle = 0.1f;

		angle += 0.01f;

		// Se crean las matrices de transformación:

		Rotation3f    rotation_x;
		Rotation3f    rotation_y;
		Translation3f translation( 0, 0, -20 );
		Projection3f  projection( 5, 15, 20, float( width ) / float( height ) );

		//rotation_x.set< Rotation3f::AROUND_THE_X_AXIS >( 0.5 );
		//rotation_y.set< Rotation3f::AROUND_THE_Y_AXIS >( angle );

		// Creación de la matriz de transformación unificada:

		// 0. Se parte de un cubo, con aristas de una unidad de longitud, centrado en (0,0)
		// 1. Se rota sobre sí mismo en el eje Y (todavía está en (0,0))
		// 2. Se rota sobre sí mismo en el eje X para que se vea un poco de canto (todavía está en (0,0))
		// 3. Se aleja hacia el fondo 10 unidades
		// 4. Se proyecta con perspectiva 

		//                                4:           3:            2:           1:
		Transformation3f transformation = projection * translation * rotation_x * rotation_y;


		for (int obj = 0; obj < rm->instance().list_meshes.size(); obj++)
		{
			// Se transforman todos los vértices usando la matriz de transformación resultante:
			// Se normalizan todos los vertices entre ( -1 y +1 ).
			//for( size_t index = 0, number_of_vertices = original_vertices.size( ); index < number_of_vertices; index++ )
			for (size_t index = 0, number_of_vertices = rm->instance().list_meshes[obj]->original_vertices.size(); index < number_of_vertices; index++)		// *****
			{
				// Se multiplican todos los vértices originales con la matriz de transformación y
				// se guarda el resultado en otro vertex buffer:

				//Vertex & vertex = transformed_vertices[ index ] = Matrix44f( transformation ) * Matrix41f( original_vertices[ index ] );
				Vertex & vertex = rm->instance().list_meshes[obj]->transformed_vertices[index] = Matrix44f(transformation) * Matrix41f(rm->instance().list_meshes[obj]->original_vertices[index]);	// *****

				// La matriz de proyección en perspectiva hace que el último componente del vector
				// transformado no tenga valor 1.0, por lo que hay que normalizarlo dividiendo:

				float divisor = 1.f / vertex[ 3 ];

				vertex[ 0 ] *= divisor;
				vertex[ 1 ] *= divisor;
				vertex[ 2 ] *= divisor;
				vertex[ 3 ] = 1.f;
			}
		}
	}

	void View::paint( )
	{
		// Se convierten las coordenadas transformadas y proyectadas a coordenadas
		// de recorte (-1 a +1) en coordenadas de pantalla con el origen centrado:

		Scaling3f        scaling = Scaling3f( float( width / 2 ), float( height / 2 ), 100000000.f );
		Translation3f    translation = Translation3f( float( width / 2 ), float( height / 2 ), 0.f );
		Transformation3f transformation = translation * scaling;

		

		rasterizer.clear();
		for (int obj = 0; obj < rm->instance().list_meshes.size(); obj++)
		{
			// Declara un objeto vector que tiene 2 valores de tipo int y que va guardar x valores dentro del vector, según el nº de vectores, lo crea de ese tamaño.
			std::vector< Point2i > rounded_vertices(rm->instance().list_meshes[obj]->transformed_vertices.size());

			// Va recorriendo cada vértice guardando la posición X e Y respecto a la pantalla. Pasando de valores normalizados a valores de pantalla.
			for (int index = 0, number_of_vertices = rm->instance().list_meshes[obj]->transformed_vertices.size(); index < number_of_vertices; index++)
			{
				//Point2i & rounded_vertex = rounded_vertices[ index ];	// Son el mismo objeto ahora. Cualquier cambio se produce en ambos, ya que son el mismo objeto.
				//Point4f & transformed_vertex = rm->instance().list_meshes[obj]->transformed_vertices[index];

				//rounded_vertex.coordinates( )[ 0 ] = int( std::floorf( transformed_vertex.coordinates( )[ 0 ] * width / 2 + 0.5f ) ) + width / 2; // Guarda el X.
				//rounded_vertex.coordinates( )[ 1 ] = int( std::floorf( transformed_vertex.coordinates( )[ 1 ] * height / 2 + 0.5f ) ) + height / 2; // Guarda el Y.

				rm->instance().list_meshes[obj]->display_vertices[index] = Point4i(Matrix44f(transformation) * Matrix41f(rm->instance().list_meshes[obj]->transformed_vertices[index]));
			}

			// Se borra el framebúffer y se dibujan los triángulos:

			//rasterizer.clear( );

			for (int index = 0, idx = 0, *indices = rm->instance().list_meshes[obj]->original_indices.data(), *end = indices + rm->instance().list_meshes[obj]->original_indices.size(); indices < end; indices += 3)
			{
				/*Point2i & rounded_vertex_0 = rounded_vertices[ original_indices[ index++ ] ];
				Point2i & rounded_vertex_1 = rounded_vertices[ original_indices[ index++ ] ];
				Point2i & rounded_vertex_2 = rounded_vertices[ original_indices[ index++ ] ];*/

			//	Point2i & rounded_vertex_0 = rounded_vertices[rm->instance().list_meshes[obj]->original_indices[index++]];		// *****
			//	Point2i & rounded_vertex_1 = rounded_vertices[rm->instance().list_meshes[obj]->original_indices[index++]];		// *****
			//	Point2i & rounded_vertex_2 = rounded_vertices[rm->instance().list_meshes[obj]->original_indices[index++]];		// *****

				/*
				// Wireframe.
				rasterizer.draw_triangle
				(
					rounded_vertex_0.coordinates( )[ 0 ], rounded_vertex_0.coordinates( )[ 1 ],
					rounded_vertex_1.coordinates( )[ 0 ], rounded_vertex_1.coordinates( )[ 1 ],
					rounded_vertex_2.coordinates( )[ 0 ], rounded_vertex_2.coordinates( )[ 1 ]
				);
				*/
				/*
				if( is_frontface( rounded_vertex_0, rounded_vertex_1, rounded_vertex_2 ) )
				{
					// rasterizer.fill_triangle( rounded_vertex_0.coordinates()[0], rounded_vertex_0.coordinates()[1], rounded_vertex_1.coordinates()[0], rounded_vertex_1.coordinates()[1],
					//	rounded_vertex_2.coordinates()[0], rounded_vertex_2.coordinates()[1], rm->instance().list_materials[obj]->original_colors[idx], rm->instance().list_materials[obj]->original_colors[idx], rm->instance().list_materials[obj]->original_colors[idx] );
					rasterizer.set_color(rm->instance().list_materials[obj]->original_colors[idx]);
					rasterizer.fill_convex_polygon_z_buffer(rm->instance().list_meshes[obj]->display_vertices.data(), indices, indices + 3);
				}
				*/
				// This method works so much better.
				if (is_frontface(rm->instance().list_meshes[obj]->transformed_vertices.data(), indices))
				{
					rasterizer.set_color(rm->instance().list_materials[obj]->original_colors[idx]);
					rasterizer.fill_convex_polygon_z_buffer(rm->instance().list_meshes[obj]->display_vertices.data(), indices, indices + 3);
				}
			
				idx++;
			}
		}

		// Se copia el framebúffer oculto en el framebúffer de la ventana:

		rasterizer.get_color_buffer( ).gl_draw_pixels( 0, 0 );
	}

	bool View::is_frontface( const Point2i v0, const Point2i v1, const Point2i v2 )
	{
		//const Vertex & v0 = projected_vertices[ indices[ 0 ] ];
		//const Vertex & v1 = projected_vertices[ indices[ 1 ] ];
		//const Vertex & v2 = projected_vertices[ indices[ 2 ] ];

		// Se asumen coordenadas proyectadas y polígonos definidos en sentido horario.
		// Se comprueba a qué lado de la línea que pasa por v0 y v1 queda el punto v2:

		return ( ( v1.coordinates( )[ 0 ] - v0.coordinates( )[ 0 ] ) * ( v2.coordinates( )[ 1 ] - v0.coordinates( )[ 1 ] ) -
		 ( v2.coordinates( )[ 0 ] - v0.coordinates( )[ 0 ] ) * ( v1.coordinates( )[ 1 ] - v0.coordinates( )[ 1 ] ) > 0.f );
	}

	bool View::is_frontface(const Vertex * const projected_vertices, const int * const indices)
	{
		const Vertex & v0 = projected_vertices[indices[0]];
		const Vertex & v1 = projected_vertices[indices[1]];
		const Vertex & v2 = projected_vertices[indices[2]];

		// Se asumen coordenadas proyectadas y polígonos definidos en sentido horario.
		// Se comprueba a qué lado de la línea que pasa por v0 y v1 queda el punto v2:

		return ((v1[0] - v0[0]) * (v2[1] - v0[1]) - (v2[0] - v0[0]) * (v1[1] - v0[1]) > 0.f);
	}
}