/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Node.h"
#include "GameObject.h"
#include "RenderManager.h"

namespace mutant
{
	Node::Node()
	:
		parentNode( nullptr )
	{
		// Initialize the transform
		transform = make_shared<Transform>();
	}

	Node::~Node()
	{
		parentNode = nullptr;
	}

	void Node::addParent( Node* node)
	{
		parentNode = node;
	}

	Node* Node::getParent()
	{
		return parentNode;
	}

	void Node::addChild( shared_ptr< Node > node)
	{
		node->addParent( this );
		childrenNode.push_back( node );
	}

	void Node::update()
	{
		// It updates the root node.
		transform->update(  true, transform->getLocalTransformation() );

		// It updates the children of the parent.
		for (vector< shared_ptr<Node> >::iterator it = childrenNode.begin(); it != childrenNode.end(); it++)
		{
			it[0]->update();
			//it[0]->Node::update();
		}

	}

	void Node::draw()
	{	
		//RenderManager::instance().rasterizer.clear();

		// It draws the children of the parent.
		for (vector< shared_ptr<Node> >::iterator it = childrenNode.begin(); it != childrenNode.end(); it++)
		{
			it[0]->draw();
		}
		//RenderManager::instance().rasterizer.get_color_buffer().gl_draw_pixels(0, 0);
	}

	shared_ptr<Transform> Node::getTransform()
	{
		return transform;
	}
}