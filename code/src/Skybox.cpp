#include "Skybox.h"
#include "SOIL.h"
#include "SceneManager.h"

#include <glm/gtc/type_ptr.hpp>
#include <iostream>

namespace mutant
{
	Skybox::Skybox(void)
	{
		//generate the cube object
		shader.loadFromFile(GL_VERTEX_SHADER, "..\\..\\..\\shaders\\skybox.vert");
		shader.loadFromFile(GL_FRAGMENT_SHADER, "..\\..\\..\\shaders\\skybox.frag");
		//compile and link shader
		shader.createAndLinkProgram();
		shader.use();
		//add shader attributes and uniforms
		shader.addAttribute("vVertex");
		shader.addUniform("MVP");
		shader.addUniform("cubeMap");
		//set constant shader uniforms at initialization
		glUniform1i(shader("cubeMap"), 0);
		shader.unUse();

		//setup the parent's fields
		Init();
	}


	Skybox::~Skybox(void)
	{

	}

	void Skybox::create()
	{
		//skybox texture ID
		GLuint skyboxTextureID;

		// Load the images. Skybox texture names.
		const char* texture_names[6] = { "../../../media/posx.png",
			"../../../media/negx.png",
			"../../../media/posy.png",
			"../../../media/negy.png",
			"../../../media/posz.png",
			"../../../media/negz.png" };

		// Load skybox textures using SOIL
		int texture_widths[6];
		int texture_heights[6];
		int channels[6];
		GLubyte* pData[6];

		cout << "Loading skybox images: ..." << endl;
		for (int i = 0; i<6; i++) {
			cout << "\tLoading: " << texture_names[i] << " ... ";
			pData[i] = SOIL_load_image(texture_names[i], &texture_widths[i], &texture_heights[i], &channels[i], SOIL_LOAD_AUTO);
			cout << "done." << endl;
		}

		//generate OpenGL texture
		glGenTextures(1, &skyboxTextureID);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTextureID);

		//set texture parameters
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		//set the image format
		GLint format = (channels[0] == 4) ? GL_RGBA : GL_RGB;

		//load the 6 images
		for (int i = 0; i<6; i++) {
			//allocate cubemap data
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, texture_widths[i], texture_heights[i], 0, format, GL_UNSIGNED_BYTE, pData[i]);

			//free SOIL image data
			SOIL_free_image_data(pData[i]);
		}

	}
	// Va en el Transform.
	void Skybox::draw()
	{
		glm::mat4 P, T, Rx, MV, S, MVP;

		glm::vec2 rotCam = SceneManager::instance().getScene()->getCamera()->GetRotation();

		P = SceneManager::instance().getScene()->getCamera()->GetProjectionMatrix();
		T = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
		Rx = glm::rotate(glm::mat4(1), rotCam[1], glm::vec3(1.0f, 0.0f, 0.0f));
		MV = glm::rotate(Rx, -rotCam[0], glm::vec3(0.0f, 1.0f, 0.0f));
		S = glm::scale(glm::mat4(1), glm::vec3(30.0));
		MVP = P*T*MV*S;

		Render(glm::value_ptr(MVP));
	}

	//there are 8 vertices in a skybox
	int Skybox::GetTotalVertices() {
		return 8;
	}

	int Skybox::GetTotalIndices() {
		//6 faces with 2 triangles each with 3 vertices
		return 6 * 2 * 3;
	}

	GLenum Skybox::GetPrimitiveType() {
		return GL_TRIANGLES;
	}

	void Skybox::FillVertexBuffer(GLfloat* pBuffer) {
		glm::vec3* vertices = (glm::vec3*)(pBuffer);
		vertices[0] = glm::vec3(-0.5f, -0.5f, -0.5f);
		vertices[1] = glm::vec3(0.5f, -0.5f, -0.5f);
		vertices[2] = glm::vec3(0.5f, 0.5f, -0.5f);
		vertices[3] = glm::vec3(-0.5f, 0.5f, -0.5f);
		vertices[4] = glm::vec3(-0.5f, -0.5f, 0.5f);
		vertices[5] = glm::vec3(0.5f, -0.5f, 0.5f);
		vertices[6] = glm::vec3(0.5f, 0.5f, 0.5f);
		vertices[7] = glm::vec3(-0.5f, 0.5f, 0.5f);
	}

	void Skybox::FillIndexBuffer(GLuint* pBuffer) {

		//fill indices array
		GLuint* id = pBuffer;

		//bottom face
		*id++ = 0; 	*id++ = 4; 	*id++ = 5;
		*id++ = 5; 	*id++ = 1; 	*id++ = 0;

		//top face
		*id++ = 3; 	*id++ = 6; 	*id++ = 7;
		*id++ = 3; 	*id++ = 2; 	*id++ = 6;

		//front face
		*id++ = 7; 	*id++ = 6; 	*id++ = 4;
		*id++ = 6; 	*id++ = 5; 	*id++ = 4;

		//back face
		*id++ = 2; 	*id++ = 3; 	*id++ = 1;
		*id++ = 3; 	*id++ = 0; 	*id++ = 1;

		//left face 
		*id++ = 3; 	*id++ = 7; 	*id++ = 0;
		*id++ = 7; 	*id++ = 4; 	*id++ = 0;

		//right face 
		*id++ = 6; 	*id++ = 2; 	*id++ = 5;
		*id++ = 2; 	*id++ = 1; 	*id++ = 5;
	}
}