/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include <iostream>

#include "Scene.h"

namespace mutant
{
	Scene::Scene()
	{
		// Isn't needed to initialize the root, when It creates a shared_ptr is initialized for default.
		// root = nullptr;

		// Initialize the "root".
		root = make_shared< Node>();
	}

	Scene::~Scene()
	{
		release();
	}

	void Scene::release()
	{
		if (root != nullptr)
		{
			root.reset();
		}
	}

	void Scene::addNode(shared_ptr< Node > node)
	{	/*
		if ( root == nullptr )
		{
			// Initialize the "root".
			root = make_shared< Node>();
		}*/

		root->addChild( node );
	}

	void Scene::update()
	{
		root->update();
	}

	void Scene::draw()
	{
		root->draw();
	}

	void Scene::setCamera(std::shared_ptr<FreeCamera> cam)
	{
		camera = cam;
	}

	std::shared_ptr<FreeCamera> Scene::getCamera()
	{
		return camera;
	}

	std::shared_ptr<Node> Scene::getRoot()
	{
		return root;
	}
}