/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include "Transform.h"

namespace mutant
{
	Transform::Transform( )
	{
		//wPosition.set( 1.0f, 1.0f, 1.0f );
		//rotation.set<Rotation3f::AROUND_THE_X_AXIS>( 1 );
		position.set( 0, 0, 0 );
		rotation.set<Rotation3f::AROUND_THE_X_AXIS>(0);
		rotation.set<Rotation3f::AROUND_THE_Y_AXIS>(0);
		rotation.set<Rotation3f::AROUND_THE_Z_AXIS>(0);
		scale.set( 1, 1, 1 );
	}

	Transform::~Transform()
	{

	}

	// We set the transform of the parent.
	void Transform::setTransformParent( shared_ptr<Transform> tParent)
	{
		if ( parent == nullptr )
			parent = make_shared< Transform >();	// Est� linea me peta en el constructor, porque?
		parent = tParent;
	}

	void Transform::setLocation(float x, float y, float z)
	{
		position.set( x, y, z );
		
		hasChanged = true;
		
	}

	const Translation3f Transform::getLocation() const
	{
		return position;
	}

	void Transform::translate(float x, float y, float z)
	{
		position.move( x, y, z );
		hasChanged = true;
	}

	// Rotates the transform in X.
	void Transform::rotateX(float angle)
	{
		rotation.set< Rotation3f::AROUND_THE_X_AXIS>( angle );
		hasChanged = true;
	}

	// Rotates the transform in Y.
	void Transform::rotateY(float angle)
	{
		rotation.set< Rotation3f::AROUND_THE_Y_AXIS>(angle);

		hasChanged = true;
	}

	// Rotates the transform in Z.
	void Transform::rotateZ(float angle)
	{
		rotation.set< Rotation3f::AROUND_THE_Z_AXIS>(angle);

		hasChanged = true;
	}

	const Rotation3f Transform::getRotation() const
	{
		return rotation;
	}

	void Transform::setScale(float x, float y, float z)
	{
		scale.set( x, y, z );

		hasChanged = true;
	}

	void Transform::setScale(float _scale)
	{
		scale.set( _scale );
		hasChanged = true;
	}

	bool Transform::hasBeenModified()
	{
		return hasChanged;
	}

	const Transformation3f& Transform::getLocalTransformation()
	{
		return localTransformation;
	}

	const Transformation3f& Transform::getWorldTransformation()
	{
		return worldTransformation;
	}

	void Transform::update(bool parent_hasChanged, Transformation3f parentTransformation)
	{
		if (parent_hasChanged || this->hasChanged)
		{
			localTransformation = position * rotation * scale;

			worldTransformation = parentTransformation * localTransformation;

			hasChanged = false;
		}
		
	}
}