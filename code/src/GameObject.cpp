/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include<iostream>

#include "GameObject.h"
#include "Component.h"
#include "ResourceManager.h"
#include "RenderManager.h"
#include "SceneManager.h"
#include "..\camera\FreeCamera.h"

#include <glm/gtc/type_ptr.hpp>

#include <Point.hpp>

namespace mutant
{
	using toolkit::Point4f;

	// Constructor for default.
	GameObject::GameObject()
	:
		material( nullptr),
		mesh( nullptr )
	{
		// We set a unique ID to the GameObject.
		idInstance = std::to_string( ++ResourceManager::instance().counterObjects );

		// We set a default name to the GameObject.
		name = "GameObject_" + idInstance;
		//std::cout << idInstance << std::endl;
	}

	// Constructor with 1 parameter. Sets the name of the GameObject.
	GameObject::GameObject(string name)
		:
		name(name),
		material(nullptr),
		mesh(nullptr)
	{
		// We set a unique ID to the GameObject.
		idInstance = ++ResourceManager::instance().counterObjects;
		
	}

	// Constructor with name of the GO, mesh and material.
	GameObject::GameObject(string name, size_t id)
		:
		name(name),
		material(nullptr),
		mesh(nullptr)
	{
		// We set a unique ID to the GameObject.
		idInstance = ++ResourceManager::instance().counterObjects;

		this->addMesh( id);
		this->addMaterial( id );
	}

	// Destructor
	GameObject::~GameObject()
	{

	}

	// Gets the name of the GameObject.
	 string GameObject::getName() const
	{
		return name;
	}

	// Gets the ID of the GameOBject.
	string GameObject::getInstanceID() const
	{
		 return idInstance;
	}

	// Adds the camera to the GameObject.
	shared_ptr<FreeCamera> GameObject::addCamera()
	 {
		 if (camera == nullptr)
			 camera = make_shared<FreeCamera>();
		 
		 return camera;
	 }

	 // Adds the Mesh Renderer to the GameObject.
	 shared_ptr<MeshRenderer> GameObject::addMeshRenderer()
	 {
		 if ( meshRenderer == nullptr )
			meshRenderer = make_shared<MeshRenderer>();

		 return meshRenderer;
	 }

	 // Adds the Mesh Renderer to the GameObject.
	 shared_ptr<MeshRenderer> GameObject::addMeshRenderer(Mesh* mes, Material* mat)
	 {
		 if (meshRenderer == nullptr)
			 meshRenderer = make_shared<MeshRenderer>( mes, mat);

		 return meshRenderer;
	 }

	Mesh* GameObject::addMesh( size_t id )
	{
		if (mesh == nullptr)
		{
			 mesh = new Mesh();
			 mesh = ResourceManager::instance().list_meshes[id]->getMesh();
		}

		return mesh;
	}

	Mesh* GameObject::getMesh() const
	{
		return mesh;
	}

	Material* GameObject::addMaterial(size_t id)
	{
		if (material == nullptr)
		{
			material = new Material();
			material = ResourceManager::instance().list_materials[id]->getMaterial();
		}

		return material;
	}

	Material* GameObject::getMaterial() const
	{
		return material;
	}

	 // It updates the GameObject. If the GameObject haven't a MeshRenderer component and If the Scene haven't a setting camera, then the GameObject isn't updated.
	 void GameObject::update( )
	 {/*
		// We check that the GO have MeshRenderer and the scene have camera.
		 if (meshRenderer != nullptr && SceneManager::instance().getScene()->getCamera() )
		 {
			// If the GO have a mesh.
			 if (meshRenderer->getMesh())
			 {
				// We declare the mesh of the GO.
				 shared_ptr<Mesh> mesh = meshRenderer->getMesh();
				 				 
				 size_t num_of_verts = mesh->original_vertices.size();
				
				// We rotate the blades of the mill.
				 if (mesh->getName() == "aspas")
				 {
					 angle += 0.01f;
					 transform->rotateZ(angle);
				 }

				 // We rotate the "Triforce".
				 if (mesh->getName() == "triforce")
				 {
					 angle += 0.02f;
					 transform->rotateY(angle);
				 }

				 // It updates the transform of the GO If the GO changed.
				 transform->update(parentNode->getTransform()->hasBeenModified(), parentNode->getTransform()->getLocalTransformation() );

				 toolkit::Transformation3f transformation;

				
				// Transformation of the vertices.
				 for (size_t index = 0; index < num_of_verts; index++)
				 {
					 Point4f& vertex = mesh->transformed_vertices[index] = Matrix44f(transformation) * Matrix41f(mesh->original_vertices[index] );

					 float divisor = 1.f / vertex[3];

					 vertex[0] *= divisor;
					 vertex[1] *= divisor;
					 vertex[2] *= divisor;
					 vertex[3] = 1.f;
					 
				 }

				 // We apply one default material If the meshrenderer haven't material.
				 if (!meshRenderer->getMaterial())
				 {
					 shared_ptr<Material> defaultMaterial = make_shared<Material>( mesh->original_indices.size() );
					 meshRenderer->setMaterial( defaultMaterial );
				 }
			 }
		 }*/
	 }

	 // It draws the GameObject. If the GameObject haven't a MeshRenderer component and If the Scene haven't a setting camera, then the GameObject isn't drawn.
	 void GameObject::draw()
	 {
		 glm::mat4 P, T, MV, MVP, S;
		 P = SceneManager::instance().getScene()->getCamera()->GetProjectionMatrix();
		 MV = SceneManager::instance().getScene()->getCamera()->GetViewMatrix();
		 T = glm::translate(glm::mat4(1.0f), position);
		 S = glm::scale(glm::mat4(1), glm::vec3(0.05f));
		 MVP = P*T*MV * S;

		 //render the chekered plane
		 if ( meshRenderer != nullptr )
			 meshRenderer->render(glm::value_ptr(MVP), glm::value_ptr(MV));

		 Node::draw();
	 }
}