/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#include <assert.h>		// assert

#include "Skybox.h"
#include "GOPlane.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "RenderManager.h"
#include "rapidxml.hpp"


#include <sstream>
#include <fstream>      // std::ifstream
#include <iostream>

using namespace rapidxml;

namespace mutant
{
	SceneManager::SceneManager()
	:
		scene( make_shared<Scene>())
	{
		//sceneGraph = std::make_shared< SceneGraph>();
	}

	SceneManager::~SceneManager()
	{
		scene->release();
	}

	SceneManager& SceneManager::instance()
	{
		static SceneManager *pInstance;

		if (pInstance == NULL)
			pInstance = new SceneManager();

		assert(pInstance);

		return *pInstance;
	}

	void SceneManager::createScene(const std::string& path)
	{
		// Elegimos el .xml que queremos abrir y esta es la ruta por defecto de los xml.
		std::string full_path = "..\\..\\..\\assets\\" + path;
		
		// Se abre el archivo xml con los datos de la escena
		std::ifstream file(full_path);
		// Se convierte a string
		std::stringstream buffer;
		buffer << file.rdbuf();
		file.close();
		std::string content(buffer.str());

		// Se crea un objeto de la librer�a rapidxml de tipo xml_document
		xml_document<> doc;
		// Se parsea el contenido de la cadena en el objeto de tipo xml_document
		doc.parse<0>(&content[0]);
		// Se cachea el nodo raiz
		xml_node<> *pRoot = doc.first_node();

		// Valor de vuelto de un atributo.
		string value;

		// We create the default skybox	.
		std::shared_ptr<Skybox> skybox(new Skybox());
		scene->addNode(skybox);
		skybox->create();

		// We create one plane.
		std::shared_ptr<GOPlane> plane(new GOPlane());
		scene->addNode(plane);
		plane = plane->createPlane(50, 50);
		
		for (xml_node<> *pSceneNode = pRoot->first_node("scene"); pSceneNode; pSceneNode = pSceneNode->next_sibling())
		{
			cout << pSceneNode->first_attribute("name")->value() << endl;
			// Recorremos los GameObjects para crear el Grafo de Escena y con sus componentes si nos interesa. En principio solo tenemos el "Transform", que es el componente que llevan todos los GOs.

			xml_node<> *pGameObjectsNode = pSceneNode->first_node("Game_Objects");

			for (xml_node<> *pGameObjectNode = pGameObjectsNode->first_node("Game_Object"); pGameObjectNode; pGameObjectNode = pGameObjectNode->next_sibling())
			{
				// Name of the GameObject.
				char* goName = pGameObjectNode->first_attribute("name")->value();

				// Father of the GameObject.
				char* goParent = pGameObjectNode->first_attribute("parent")->value();

				// ID of the mesh & material of the GameObject.
				value = pGameObjectNode->first_attribute("id")->value();
				// Convertimos el string a size_t
				std::istringstream _id(value); 	size_t id;	 _id >> id;


				// Creamos cada GameObject declarado en el xml con su respectivo nombre y padre.
				std::shared_ptr< GameObject > newGameObject(new GameObject( goName, id ) );

				// Recorremos los componentes que tiene un GameObject
				for (xml_node<> *pComponentNode = pGameObjectNode->first_node("Component"); pComponentNode; pComponentNode = pComponentNode->next_sibling())
				{
					string typeComponent = pComponentNode->first_attribute("name")->value();

					// En caso de que el GO tenga un component Mesh Renderer, le a�adimos una malla y un material.
					if (typeComponent == "Mesh_Renderer")
					{
						// Set the mesh and the material that the GO have himself.
						newGameObject->addMeshRenderer( newGameObject->getMesh(), newGameObject->getMaterial() );
					}

					// En caso de que el GO tenga un transform con alguna configuraci�n que no sea la predeterminada con el Transform por defecto.
					// Si no se pasa un transform, pone uno por defecto. Postion( 0, 0, 0 ) Rotation ( 0, 0, 0 ) Scale ( 1, 1, 1 )
					if (typeComponent == "Transform")
					{	
						// POSITION
						xml_node<> *positionNode = pComponentNode->first_node("Position");
						if ( positionNode != nullptr )
						{
							// We get the position "X" of the GO
							value = positionNode->first_attribute("x")->value();
							float pos_x = (float)atof(value.c_str());

							// We get the position "Y" of the GO
							value = positionNode->first_attribute("y")->value();
							float pos_y = (float)atof(value.c_str());

							// We get the position "Z" of the GO
							value = positionNode->first_attribute("z")->value();
							float pos_z = (float)atof(value.c_str());

							// We set the position to the GO from the options of XML.
							newGameObject->position[0] = pos_x;
							newGameObject->position[1] = pos_y;
							newGameObject->position[2] = pos_z;
						}


						// ROTATION
						xml_node<> *rotationNode = pComponentNode->first_node("Rotation");
						// We check that the GO have rotation.
						if (rotationNode != nullptr)
						{
							// We get the rotation "X" of the GO
							value = rotationNode->first_attribute("x")->value();
							std::istringstream rx(value); 	float rot_x;	 rx >> rot_x;

							// We get the rotation "Y" of the GO
							value = rotationNode->first_attribute("y")->value();
							std::istringstream ry(value); 	float rot_y;	 ry >> rot_y;

							// We get the rotation "Z" of the GO
							value = rotationNode->first_attribute("z")->value();
							std::istringstream rz(value); 	float rot_z;	 rz >> rot_z;

							if ( rot_x != 0 )
								// We set the rotation to the GO in the axis X from the options of XML.
								newGameObject->getTransform()->rotateX( rot_x );

							if (rot_y != 0)
								// We set the rotation to the GO in the axis Y from the options of XML.
								newGameObject->getTransform()->rotateY(rot_y);

							if (rot_z != 0)
								// We set the rotation to the GO in the axis Z from the options of XML.
								newGameObject->getTransform()->rotateZ(rot_z);
						}

						// SCALE

						// For now we haven't set up the scale for XML. This will be done in the future.
					}

					if (typeComponent == "Material")
					{
						// ID of the mesh & material of the GameObject.
						value = pComponentNode->first_attribute("id_texture")->value();
						// Convertimos el string a size_t
						std::istringstream _id_texture(value); 	size_t id_texture;	 _id_texture >> id_texture;

						newGameObject->getMaterial()->setTexture(ResourceManager::instance().list_textures[id_texture]);
					}
				}
				// We add the new GameObject to the parent.
				scene->addNode(newGameObject);
			}
		}
		
		

	//	std::shared_ptr< GameObject > Box(new GameObject("BOX", 0));
	//	Box->addMeshRenderer(Box->getMesh(), Box->getMaterial());
		//Box->getMaterial()->setTexture(ResourceManager::instance().list_textures[0]);
	//	scene->addNode(Box);
		// We create and set up the main camera.
		std::shared_ptr< GameObject > camera(new GameObject("Camara"));
		scene->addNode(camera);
		scene->setCamera(camera->addCamera());
		vector<size_t> display = RenderManager::instance().getRenderWindow()->getResolution();
		glViewport(0, 0, (GLsizei)display[0], (GLsizei)display[1]);
		scene->getCamera()->SetPosition(glm::vec3(1));
		scene->getCamera()->SetupProjection(45, ((GLfloat)display[0] / display[1]), 1.f, 100.f);
	}

	std::shared_ptr< Scene > SceneManager::getScene()
	{
		return scene;
	}
}