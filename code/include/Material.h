/* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * *\
*																				*
*		Javier Barreiro Portela													*
*																				*
*		22 April 2015  - Programmning Graphics									*
*																				*
*		Design & Develop of Videogames											*
*																				*
*		jbarreiro.23@gmail.com													*
*																				*
\* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

#ifndef MATERIAL_HEADER
#define MATERIAL_HEADER

#include <vector>
#include <memory>

#include "GLSLShader.h"
#include "Texture.h"

namespace mutant
{
	class Material
	{
	private:
		// Name of the material.
		std::string name;

		// ID of the material.
		const int id;

		// Texture of the material.
		shared_ptr< Texture > texture;

		// Shader
		GLSLShader shader;

	public:
		// Constructor to create a new material.
		Material( int id, std::string name );

		// Default constructor
		Material();

		// Constructor to create a default material.
		Material( size_t size );

		// Destructor.
		virtual ~Material();

		// We gets the name of the GO.
		std::string getName();

		// We gets the ID of the GO.
		int getID();

		// We get the material original.
		Material* getMaterial();

		// We set the texture of the material.
		void setTexture( shared_ptr< Texture > textur );

		// We get the texture of the material.
		shared_ptr< Texture > getTexture() const;

		// We set the shader of material.
		void setShader(GLSLShader shad );

		// We get the sahder of material.
		GLSLShader getShader() const;
	};
}

#endif;