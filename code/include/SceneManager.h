/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef SCENEMANAGER_HEADER
#define SCENEMANAGER_HEADER

#include "Scene.h"

#include <vector>

namespace mutant
{

	class SceneManager
	{
	private:
		// Destroys the instance when exits of the app.
		static void DestroyInstance();

		std::shared_ptr< Scene > scene;

		SceneManager();

	public:
		// Destroyer
		~SceneManager();

		// Singleton
		static SceneManager& instance( );

		// Creates one scene passing a determinate xml.
		void createScene( const std::string& path );

		// We get the scene.
		std::shared_ptr< Scene > getScene();
	};

}

#endif;