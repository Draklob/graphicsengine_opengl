
#pragma once
#include <GL/glew.h>
#include <map>
#include <string>

#ifndef _GLSLSHADER_H_
#define _GLSLSHADER_H_

using namespace std;

namespace mutant
{
	class GLSLShader
	{
		public:
			// Void as function parameter let us to achieve the same interpretation across both languages and make our headers multilingual.
			// Constructor
			GLSLShader( void );
			// Destructor
			virtual ~GLSLShader( void );

			// Handle the shader compilation, linking and program creation.
			void loadFromString( GLenum whichShader, const string& source );
			void loadFromFile( GLenum whichShader, const string& filename );
			void createAndLinkProgram();

			// Bind and unbind a program.
			void use();
			void unUse();

			// Add the localtion of the attribute and uniforms into their respective map.
			void addAttribute( const string& attribute );
			void addUniform( const string& uniform );

			// Two indexers to access the attribute and uniform locations from their maps. Overload [] for uniforms and overload the parenthesis operation ().
			GLuint operator[ ] ( const string& attribute );
			GLuint operator( ) ( const string& uniform );

			// We delete of the shader program object.
			void deleteShaderProgram( );

		private:
			enum ShaderType{ VERTEX_SHADER, FRAGMENT_SHADER, GEOMETRY_SHADER };
			GLuint _program;
			int _totalShaders;
			GLuint _shaders[3];

			// Two std::map datastructure to store the attribute's/uniform's name as the key and its location as the value.
			// This is done to remove the redundant call to get the attribute's/uniform's location each frame or when the location is required to access the attribute / uniform.
			map< string, GLuint > _attributeList;
			map< string, GLuint > _uniformLocationList;
	};
}
	#endif /* _GLSLSHADER_H */

	/*
		Typical Shader Application:
			1 - Create the GLSLShader object either on stack "GLSLSHADER shader;" or on the heap "GLSLShader* shader = new GLSLShader();".
			2 - Call loadFromFile on the GLSLShader object reference.
			3 - Call createAndLinkProgram on the GLSLShader object reference.
			4 - Call use on the GLSLShader object reference to bind the shader object.
			5 - Call AddAttribute/AddUniform to store locations of all of the shader's attributes and uniforms respectively.
			6 - Call UnUse on the GLSLShader object reference to unbind the shader object.

			Note that the above steps are required at initialization only. We can set the values of the uniforms
			that remain constant throughout the execution of the application in the Use/UnUse block given above.

			At the rendering step, we access uniform(s), if we have uniforms that change each frame (for example,
			the modelview matrices). We fist bind the shader by calling the GLSLShader::Use function. We then set
			the uniform by calling the glUniform{*}  function, invoke the rendering by calling the glDraw{*} function,
			and then unbind the shader (GLSLShader::UnUse). Note that the glDraw{*} call passes the attributes to the GPU.

			In a typical OpenGL shader application, the shader specific functions and their sequence of execution are as follows:
			
				-	glCreateShader
				-	glShaderSource
				-	glCompileShader
				-	glGetShaderInfoLog

				These functions are the first four steps handled in the "loadFromString" function.

			Execution of the above four functions creates a shader object. After the shader object is created,
			a shader program object is created using the following set of functions in the following sequence:
		
				-	glCreateProgram
				-	glAttachShader
				-	glLinkProgram
				-	glGetProgramInfoLog

				These functions are the first four steps handled in the "createAndLinkProgram" function.

			*** Note that after the shader program has been linked, we can safely delete the shader object. ***

			Then of do of the above, the shader program object has been creates, we can set the program for execution on the GPU. This
			process is called "Shader binding". This is carried out by the "glUseProgram" function which is called through the "use / unUse" functions.

			To enable communication between the application and the shader, there are two differente kinds of fields avaible in the shader. The first are the
			attributes which may change during shader execution across diferent shader stages. All per-vertex attributes fall in this category. The second are
			the uniforms which remain constant throughout the shader execution. Typical examples include the modelview matrix and the texture samplers.

			In order to communicate with the shader program, the application must obtain the location of an attribute / uniform after the shader program is bound.
			The location identifies the attribute / uniform. In the GLSLShader class, for convenience, we store the locations of attributes and uniforms in two maps.

			For accessing any attribute / uniform location, we provide an indexer here. In cases where there is an error in the compilation or linking stage, the shader log is printed to the console.
	*/

	/*
		Simple Triangle

		The vertex position is specified in what is called the object space. This space is the one in which the vertex location is specified for an object.
		We apply modeling transformation to the object space vertex position by multiplying it with an affine matrix ( for example, a matrix for scaling, rotating, translating ... ).
		This brings the object space vertex position into world space. Next, the world space positions are multiplied bye the camera / viewing matrix which brings the position into
		view / eye / camera space. OpenGL stores the modeling and viewing transformations in a single ( modelview ) matrix. The view space positions are then projected by using a projection
		transformation which brings the position into clip space. The clip space positions are then normalized to get the normalized device coordinates which have a canonical viewing volume
		(coordinates are [-1,-1,0] to [1,1,1] in x, y, and z coordinates respectively). Finally, the viewport transformation is applied which brings the vertex into window/screen space.
	*/