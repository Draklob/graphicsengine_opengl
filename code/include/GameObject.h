/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef GAMEOBJECT_HEADER
#define GAMEOBJECT_HEADER

#include <string>
#include <vector>
#include <Shared_Ptr.hpp>

#include "Node.h"
#include "..\camera\FreeCamera.h"
#include "MeshRenderer.h"

namespace mutant
{
	using namespace std;

	class Component;
	class ResourceManager;

	// This class is inherited of Node, of this mode can create the Scene Graph directly with GameObjects.
	class GameObject : public Node
	{
	private:
		typedef Point4f               Vertex;

		// Name of the GameObject.
		string name;

		// Instance ID of the GameObject.
		string idInstance;

		// Vector of components of the GameObject.
		vector< shared_ptr< Component > > components;

		// We need one instance of ResourceManager, where the meshes and materials are loaded.
		ResourceManager *rm;

		// THIS IS PROVISIONAL WHEREAS I CREATE ONE GOOD SYSTEM TO WORK WITH COMPONENTS.

		// Component "MeshRenderer" of the object.
		shared_ptr<MeshRenderer> meshRenderer;

		// Component "Camera" of the object.
		shared_ptr<FreeCamera> camera;

		// Component "Mesh" of the object.
		Mesh* mesh;

		// Component "Material" of the object.
		Material* material;

		// The used angle to rotate the blades of the mill.
		float angle = 0.1f;

	public:
		// Position of the GameObject. THIS IS TEMPORAL. It will in the transform.
		glm::vec3 position;

		// Constructor for default.
		GameObject( );

		// Constructor to set a name to the GameObject.
		GameObject( string name );

		// Constructor to set a name to the GameObject, a mesh and a material.
		GameObject(string name, size_t id);

		// Destructor
		virtual ~GameObject();

		// Get the name of the GameObject.
		string getName() const;

		// Get the Istance ID of the GameObject.
		string getInstanceID() const;

		// Add a component to the GameObject.
		void addComponent( shared_ptr<Component> comp);

		// Remove a component to the GameObject.
		void removeComponent( shared_ptr<Component> comp);

		// Get a component of the GameObject for ID.
		shared_ptr<Component> getComponent(string ID) const;

		// Show all components of the GameObject.

		// Check if the GameObject have a specific component.
		bool hasComponent( string ID );

		// Destroy the GameObject
		void Destroy();

		// Adds the camera component to the GameObject.
		shared_ptr<FreeCamera> addCamera();

		// Adds the meshrenderer component to the GameObject.
		shared_ptr<MeshRenderer> addMeshRenderer();

		// Adds the meshrenderer component to the GameObject.
		shared_ptr<MeshRenderer> addMeshRenderer( Mesh* mesh, Material* mat);

		// We add the material component to the GameObject.
		Material* addMaterial( size_t id );

		// We get the material applied to the GO.
		Material* getMaterial() const;

		// We add the mesh component to the GameObject.
		Mesh* addMesh(size_t id );

		// We get the mesh applied to the GO.
		Mesh* getMesh() const;

		// Updates the GameObject
		void update( );

		// Paints the GameObject.
		void draw();
	};
}

#endif;