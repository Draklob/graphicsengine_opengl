/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Render Window												*
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef RENDERWINDOW_HEADER
#define RENDERWINDOW_HEADER

#include "SceneManager.h"

namespace mutant
{
	using namespace std;

	class RenderWindow
	{

	private:
		// Width of the display.
		size_t window_width;

		// Height of the display.
		size_t window_height;

		// Window of the application.
		SDL_Window* window;

		// The window renderer
		SDL_Renderer* renderer;

		// Scene
		shared_ptr<Scene> scene = make_shared<Scene>();

		//delta time
		float dt;

		//timing related variables
		float lastFrameTime, currentFrameTime;

		void initialize();

	public:
		// Method to render the application.
		bool render();

		// Constructor
		RenderWindow( );

		// En caso de que queramos establecer nosotros directamente una resolución.
		RenderWindow( const size_t width, const size_t height);

		// Destructor
		~RenderWindow();
		
		// We set the resolution of the window.
		void setResolution( const size_t width, const size_t height );

		// We get the resolution of the window.
		vector<size_t> getResolution();

		// We get the window.
		SDL_Window* getWindow();

		// We get the renderer.
		SDL_Renderer* getRenderer();
	};
}


#endif;