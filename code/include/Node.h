/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef NODE_HEADER
#define NODE_HEADER

#include <memory>
#include <vector>

#include "Transform.h"


namespace mutant
{	
	using namespace std;

	class Node 
	{
	protected:
		// Parent of the node.
		Node* parentNode;

		// Children of the node.
		vector< shared_ptr< Node > > childrenNode;

		// Transform of the object.
		shared_ptr<Transform> transform;

	public:
		// Constructor
		Node();

		// Destructor
		virtual ~Node();

		// We add a parent to the node.
		void addParent( Node* );

		// We add a child to the node.
		void addChild( shared_ptr<Node> );

		// Updates the node.
		virtual void update( );

		// Draws the properties of the node.
		virtual void draw();

		// Return the parent node.
		Node* getParent();

		// Detach the children to destroy the root ( parent ) without destroying the children.
		void detachChildren();

		// We get the transform.
		// Transform of the object.
		shared_ptr<Transform> getTransform();
	};
}
#endif;