/* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * *\
*																				*
*		Javier Barreiro Portela													*
*																				*
*		22 April 2015  - Programmning Graphics									*
*																				*
*		Design & Develop of Videogames											*
*																				*
*		jbarreiro.23@gmail.com													*
*																				*
\* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

#ifndef APPLICATION_HEADER
#define APPLICATION_HEADER

#include <SDL_image.h>

#include <GL/glew.h>

#include <memory>	// shared_ptr

#include "RenderManager.h"
#include "ResourceManager.h"
#include "SceneManager.h"

#include <SDL.h>
#include <SDL_image.h>


namespace mutant
{
	using std::shared_ptr;

	class Application
	{
	private:
		// The used systems when the application is initialized.
		ResourceManager *recM;
		RenderManager* renM;
		SceneManager* scenM;

		SDL_GLContext context;
		
	public:
		// Constructor
		Application();

		//Destructor
		~Application();

		// Method that initialize the application.
		void launch( std::string scene );
//		void launch();

	private:
		bool init();
	};

}

#endif;