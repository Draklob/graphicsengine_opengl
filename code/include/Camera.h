/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef CAMERA_HEADER
#define CAMERA_HEADER

#include "Component.h"
#include "Projection.hpp"

namespace mutant
{
	using toolkit::Projection3f;

	// Component of type "Camera" that inherit of the parent class "Component".
	class Camera : public Component
	{
	private:
		// Projection of the camera. In the future this can change, there are differents types of cameras.
		Projection3f projection;

	public:
		// Constructor for the projection in mode perspective.
		Camera( float near_plane, float far_plane, float fov, float width, float height );

		// Destructor
		virtual ~Camera( );

		// Sets the projection of the camera.
		void setProjection( const float& near, const float& far, const float& fov, const float& width, const float& height );

		// Returns the projection of the camera.
		const Projection3f& getProjection() const;
	};
}

#endif;