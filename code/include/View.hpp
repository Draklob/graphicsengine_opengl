/* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * *\
*																				*
*		Javier Barreiro Portela													*
*																				*
*		22 April 2015  - Programmning Graphics									*
*																				*
*		Design & Develop of Videogames											*
*																				*
*		jbarreiro.23@gmail.com													*
*																				*
\* * * * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * * * */

#ifndef VIEW_HEADER
#define VIEW_HEADER

#include <vector>
#include <Point.hpp>
#include "Color_Buffer_Rgb565.hpp"
#include "Color_Buffer_Rgba8888.hpp"
#include "ResourceManager.h"

namespace mutant
{
	using std::vector;
	using toolkit::Point4f;
	using toolkit::Point2i;

	class View
	{
	private:
		typedef Color_Buffer_Rgba8888   Color_Buffer;
		typedef Color_Buffer::Color		Color;			// Guardamos un tipo por defecto que referencia a la estructura de Color_Buffer_Rgba8888.
		typedef Point4f					Vertex;
		typedef vector< Vertex >		Vertex_Buffer;
		typedef vector< int >			Index_Buffer;
		typedef vector< Color >			Vertex_Colors;
		
	private:
		size_t width;
		size_t height;

		Color_Buffer               Color_buffer;
	//	Rasterizer< Color_Buffer > rasterizer;

		Vertex_Buffer original_vertices;
		Index_Buffer  original_indices;
		Vertex_Buffer transformed_vertices;
		Vertex_Colors original_colors;
		vector< Point4i > display_vertices;

		ResourceManager *rm;

	public:

		View( size_t width, size_t height );

		void update( );
		void paint( );

	private:

		bool is_frontface( Point2i v0, Point2i v1, Point2i v2 );
		bool is_frontface(const Vertex * const projected_vertices, const int * const indices);
	};
}

#endif;