/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef SCENE_HEADER
#define SCENE_HEADER

#include <memory>

#include "Node.h"
#include "..\camera\FreeCamera.h"

namespace mutant
{
	class Scene
	{
	private:
		// Root of the Scene Graph.
		std::shared_ptr<Node> root;

		

	public:
		// Constructor
		Scene();

		// Destructor
		virtual ~Scene();

		// Remove the root of the SceneGraph and reset the root.
		void release();

		// We add one node to the SceneGraph.
		void addNode( std::shared_ptr<Node> );

		// Updates all the Scene Graph.
		void update();

		// Draws all the Scene Graph.
		void draw();

		void setCamera(std::shared_ptr<FreeCamera> camera);

		// We get the main camera of the scene.
		std::shared_ptr<FreeCamera> getCamera();

		// We get the root node of the scene.
		std::shared_ptr<Node> getRoot();

		// Camera that needs the scene to draw and update the GameObjects.
		std::shared_ptr<FreeCamera> camera;
	};
}

#endif;