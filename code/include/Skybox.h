#pragma once
#include "RenderableObject.h"

#include <memory>

#include "Node.h"

namespace mutant
{
	class Skybox :public RenderableObject, public Node
	{
	public:
		Skybox(void);
		virtual ~Skybox(void);

		int GetTotalVertices();
		int GetTotalIndices();
		GLenum GetPrimitiveType();
		void FillVertexBuffer(GLfloat* pBuffer);
		void FillIndexBuffer(GLuint* pBuffer);

		void create();

		void draw();
	};
}
