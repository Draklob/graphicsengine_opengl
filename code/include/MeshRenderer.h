/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef MESHRENDERER_HEADER
#define MESHRENDERER_HEADER

#include <memory>
#include <GL\glew.h>

#include "Component.h"


namespace mutant
{
	using namespace std;

	class Material;
	class Mesh;

	// MeshRenderer inherits of Component.
	class MeshRenderer : public Component
	{
	private:
		enum MeshBufferPositions
		{
			POSITION_VB,    //0
			TEXCOORD_VB,    //1
			NORMAL_VB,	//2
			INDEX_VB,       //3
			NUM_BUFFERS     //4
		};

		// Material that use the GO.
		Material* material;

		// Mesh that use the GO.
		Mesh* mesh;

		// Render data
		GLuint vaoID, vboVerticesID, vboIndicesID, vAB[NUM_BUFFERS];

		int totalVertices, totalIndices;

		void setupMesh();

	public:
		// Constructor
		MeshRenderer();

		// Constructor with a material and a mesh.
		MeshRenderer(Mesh* mesh, Material* material);

		// Destructor
		virtual ~MeshRenderer();

		// We set the material for the GO.
		void setMaterial( Material* material );

		// We set the mesh for the GO.
		void setMesh( Mesh* mesh  );

		void render(const GLfloat* MVP, const GLfloat* MV);
	};
}

 #endif;