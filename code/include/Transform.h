/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef TRANSFORM_HEADER
#define TRANSFORM_HEADER

#include <cmath>
#include <cassert>
#include <iostream>
#include <memory>
#include "Vector.hpp"

#include "Scaling.hpp"
#include "Rotation.hpp"
#include "Projection.hpp"
#include "Translation.hpp"
#include "Point.hpp"

#include "Component.h"

using toolkit::Translation3f;
using toolkit::Rotation3f;
using toolkit::Scaling3f;
using toolkit::Point4f;

namespace mutant
{
	using namespace toolkit;
	using namespace std;

	class Transform : public Component
	{
	private:
		// Position of the transform.
		Translation3f position;

		// Rotation in world space.
		Rotation3f rotation;

		// Scale in world space.
		Scaling3f scale;

		// Parent's transform.
		shared_ptr<Transform> parent;

		// Changed the transform ?
		bool hasChanged = false;

		// Local Transformation 
		Transformation3f localTransformation;

		// World Transformation
		Transformation3f worldTransformation;

	public:
		// Constructor
		Transform( );

		// Destructor
		virtual ~Transform();

		// We set the transform of the parent.
		void setTransformParent(shared_ptr<Transform> tParent);

		// We get the Localtransformation of the parent.
		const Transformation3f& getLocalTransformation();

		// We get the World transformation of the parent.
		const Transformation3f& getWorldTransformation();

		// Set Location
		void setLocation(float x, float y, float z);

		// Get Location
		const Translation3f getLocation() const;

		// Translate the object.
		void translate( float x, float y, float z );

		// Rotates the transform in X.
		void rotateX( float angle );

		// Rotates the transform in Y.
		void rotateY( float angle);

		// Rotates the transform in Z.
		void rotateZ( float angle);

		// Get Rotation
		const Rotation3f getRotation() const;

		// We scale the object.
		void setScale( float x, float y, float z );

		void setScale( float scale );

		// The transform has been modified.
		bool hasBeenModified();

		// It updates the transform of the GO.
		void update(bool parent_hasChanged, Transformation3f parentTransform );
	};
}

#endif;
