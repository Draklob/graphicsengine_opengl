/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

// This class manages the game's resources ( assets or media ), examples as meshes, materials, textures, shader programs...
// A game's resources must be managed, both in terms of the offline tools used to create them and in terms of loading,
// unloading and manipulatin them at runtime.
// Two components: 
//		- One component manages the chain of offline tools to create them the assets.
//		- The other component manages the resources at runtime, ensuring that they are loaded into memory when they are needed and unloaded when aren't needed.

#ifndef RESOURCEMANAGER_HEADER
#define RESOURCEMANAGER_HEADER

#include "../../lib/tiny_loader/tiny_obj_loader.h"
#include "Mesh.h"
#include "Material.h"
#include "Texture.h"

#include <glm\glm.hpp>
#include <memory>


#include <stdio.h>

namespace mutant
{
	using namespace std;

	using tinyobj::shape_t;
	using tinyobj::material_t;

	class ResourceManager
	{
	private:

		ResourceManager();
		
		// List of the shapes.
		vector<shape_t> shapes;

		// List of the materials.
		vector<material_t> materials;

		// Method to create a mesh of one object.
		void createMesh(tinyobj::mesh_t mesh, int idObject);

		// Method to create a material of one object.
		void createMaterial( int material, int obj );

		// Method to create a texture of one object.

	public:
		// Destroyer
		~ResourceManager();

		// List of saved meshes.
		vector< shared_ptr< Mesh > > list_meshes;

		// List of saved materials.
		vector< shared_ptr< Material > > list_materials;

		// List of saved textures.
		vector< shared_ptr< Texture > > list_textures;

		// We load objets.
		void loadObjects( string inputFile);

		// Counter of all the objects.
		int counterObjects;

		// Singleton
		static ResourceManager& instance();
		
		// We load one object
		void loadObject(const char * pathObj);

		// We load one texture.
		void loadTexture(const string inputFile );
	};
}

#endif;