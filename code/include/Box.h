#pragma once
#include "RenderableObject.h"
#include "Node.h"

#include <memory>

namespace mutant
{
	class Box
		: public RenderableObject, public Node
	{
	public:
		Box();
		virtual ~Box(void);
		void create();

		void draw();
	};
}