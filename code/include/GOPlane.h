#pragma once
#include "RenderableObject.h"
#include "Node.h"

#include <memory>

namespace mutant
{
	class GOPlane
	: public RenderableObject, public Node
	{
	public:
		GOPlane(const int width = 1000, const int depth = 1000);
		virtual ~GOPlane(void);
		int GetTotalVertices();
		int GetTotalIndices();
		GLenum GetPrimitiveType();

		void FillVertexBuffer(GLfloat* pBuffer);
		void FillIndexBuffer(GLuint* pBuffer);

		shared_ptr<GOPlane> createPlane(int width, int height);

		void draw();

	private:
		int width, depth;
	};
}