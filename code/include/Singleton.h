/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
 *																		*
 *			Date: 15-06-15												*
 *																		*
 *			Mutant Engine												*
 *																		*
 *			Javier Barreiro Portela										*
 *																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef SINGLETON_HEADER
#define SINGLETON_HEADER

namespace mutant{
	
	class Singleton
	{
		public:
			static Singleton& getInstance();
			void showMessage() const;
			virtual ~Singleton(){};
		private:
			static Singleton *pInstance;

			Singleton() { };

			Singleton( const Singleton & );
			Singleton & operator=( const Singleton& );

			static void DestroySingleton();
	};

}

#endif; // SINGLETON_HEADER