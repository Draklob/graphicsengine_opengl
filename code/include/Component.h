/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef COMPONENT_HEADER
#define COMPONENT_HEADER

#include <string>
#include <memory>

namespace mutant
{
	using namespace std;

	class GameObject;

	class Component
	{
	protected:
		// Unique ID for the component.
		string ID;

		shared_ptr<GameObject> gameObject;
		//GameObject *go;

	public:
		// We get the ID of the component.
		string getComponentID();

		// We set the GameObject that have this component.
		void setGameObject( shared_ptr<GameObject> gameObject );

		// We get the GameObject that have the component attached.
		shared_ptr<GameObject> getGameObject() const;
	};
}

#endif;