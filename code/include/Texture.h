/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <GL/glew.h>

namespace mutant
{
	class Texture
	{
	public:
		Texture( const int id, const std::string name );
		virtual ~Texture();

		void bind( );

		std::string getName() const;

		int getID( ) const;

		void setSurface( SDL_Surface* surface );

		SDL_Surface* getSurface() const;

		void setTexture( GLuint texture );

		GLuint getTexture() const;

	private:
		// Name of the texture.
		const std::string name;

		// ID of the texture.
		const int id;

		SDL_Surface* m_Texture;
		GLuint texture;
	};
}

#endif _TEXTURE_H