#include "FreeCamera.h"
#include <glm/gtx/euler_angles.hpp>

namespace mutant
{

	FreeCamera::FreeCamera()
	{
		translation = glm::vec3(0);
		speed = 0.5f; // 0.5 m/s
	}


	FreeCamera::~FreeCamera(void)
	{
	}

	void FreeCamera::Update() {
		glm::mat4 R = glm::yawPitchRoll(yaw, pitch, roll);
		position += translation;

		//set this when no movement decay is needed
		translation=glm::vec3(0); 

		// Trasnform the look vector by the current rotation matrix and determine the right and up vectors to calculate the orthonormal basis.
		look = glm::vec3(R*glm::vec4(0, 0, 1, 0));
		up = glm::vec3(R*glm::vec4(0, 1, 0, 0));
		right = glm::cross(look, up);

		// Determine the camera target point.
		glm::vec3 tgt = position + look;
		// Use the glm::lokat function to calculate the new view matrix using the camera position, target and the up vector.
		V = glm::lookAt(position, tgt, up);
	}




	void FreeCamera::Walk(const float dt) {
		translation += (look*speed*dt);
		Update();
	}

	void FreeCamera::Strafe(const float dt) {
		translation += (right*speed*dt);
		Update();
	}

	void FreeCamera::Lift(const float dt) {
		translation += (up*speed*dt);
		Update();
	}

	void FreeCamera::SetTranslation(const glm::vec3& t) {
		translation = t;
		Update();
	}

	glm::vec3 FreeCamera::GetTranslation() const {
		return translation;
	}

	void FreeCamera::SetSpeed(const float s) {
		speed = s;
	}

	const float FreeCamera::GetSpeed() const {
		return speed;
	}

}