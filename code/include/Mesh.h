/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

#ifndef MESH_HEADER
#define MESH_HEADER

#include <vector>
#include <Point.hpp>

#include <glm\glm.hpp>
#include <GL\glew.h>

namespace mutant
{
	class Mesh
	{
	/*private:
		typedef std::vector<glm::vec3> Vec3;
		typedef std::vector<glm::vec2> Vec2;
		typedef std::vector<GLushort> USint;

	public:
		Vec3	position;
		Vec2	UVs;
		Vec3	normals;
		USint	indices;*/

	private:
		typedef std::vector<GLfloat> Vec3;
		typedef std::vector<GLfloat> Vec2;
		typedef std::vector<GLushort> USint;

	public:
		Vec3	position;
		Vec2	UVs;
		Vec3	normals;
		USint	indices;

	private:
		// Name of the Mesh.
		std::string name;

		// ID of the Mesh.
		const int id;

	public:
		// Constructor
		Mesh(int id, std::string name);

		// Default constructor
		Mesh();

		// Destructor
		virtual ~Mesh();

		// We get the name of the mesh.
		std::string getName();

		// We get the ID of the mesh.
		int getID();

		// We set the vertices of the mesh.
		void setVertices( Vec3 vertices);

		// We get the vertices of the mesh.
		Vec3 getVertices();

		// We set the UVs of the mesh.
		void setUVs(Vec2 uvs);

		// We get the UVs of the mesh.
		Vec2 getUVs();

		// We set the normals of the mesh.
		void setNormals(Vec3 normals);

		// We get the normals of the mesh.
		Vec3 getNormals();

		// We set the indices of the mesh.
		void setIndices(USint USint);

		// We get the indices of the mesh.
		USint getIndices();

		// We get the mesh.
		Mesh* getMesh();
	};
}

#endif;