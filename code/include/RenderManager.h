/* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * *\
*																		*
*			Render Manager												*
*																		*
*			Date: 15-06-15												*
*																		*
*			Mutant Engine												*
*																		*
*			Javier Barreiro Portela										*
*																		*
\* * * * * * * * * * * * * * * * * ** * * * * * * * * * * * * * * * * * */

// This class "RenderManager" handles all the settings of the rendering process.

#ifndef RENDERMANAGER_HEADER
#define RENDERMANAGER_HEADER

#include "RenderWindow.h"

namespace mutant
{
	class RenderManager
	{
	public:
		// Destroyer
		~RenderManager();

		// Singleton
		static RenderManager& instance();

		// It initializes this class.
		void startUp();

		// It finishes this class. With this can delete all calling to the destructor.
		void shutDown();

		// We get the RenderWindow
		shared_ptr<RenderWindow> getRenderWindow();

	private:
		// Constructor
		RenderManager();
			
		// Destroys the instance when exits of the app.
		static void DestroyInstance();

		// Reference of RenderWindow.
		shared_ptr<RenderWindow> win;
	};
}

#endif;