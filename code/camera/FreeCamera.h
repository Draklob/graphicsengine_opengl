#pragma once
#include "AbstractCamera.h"
#include "Component.h"

#include <SDL.h>

namespace mutant
{
	class FreeCamera :
		public AbstractCamera, Component
	{
		public:
			FreeCamera(void);
			~FreeCamera(void);

			void Update();

			void Walk(const float dt);
			void Strafe(const float dt);
			void Lift(const float dt);

			void SetTranslation(const glm::vec3& t);
			glm::vec3 GetTranslation() const;

			void SetSpeed(const float speed);
			const float GetSpeed() const;

			void OnMouseMove(int button, int x, int y);
			void OnMouseDown(int button, int x, int y);
			void moveCamera(Uint16 key, float dt);

			glm::vec2 GetRotation() const;

	protected:

		float speed;			//move speed of camera in m/s
		glm::vec3 translation;

		//camera tranformation variables
		int state = 0, oldX = 0, oldY = 0;
		float rX = 0, rY = 0, fov = 45;
		//delta time
		float speedC = 12;
	};
}