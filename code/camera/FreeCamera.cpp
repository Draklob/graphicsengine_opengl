#include "FreeCamera.h"
#include <glm/gtx/euler_angles.hpp>

namespace mutant
{
	FreeCamera::FreeCamera()
	{
		translation = glm::vec3(0);
		speed = 0.5f; // 0.5 m/s
	}


	FreeCamera::~FreeCamera(void)
	{
	}

	void FreeCamera::Update() {
		glm::mat4 R = glm::yawPitchRoll(yaw, pitch, roll);
		position += translation;

		//set this when no movement decay is needed
		translation=glm::vec3(0); 

		// Trasnform the look vector by the current rotation matrix and determine the right and up vectors to calculate the orthonormal basis.
		look = glm::vec3(R*glm::vec4(0, 0, 1, 0));
		up = glm::vec3(R*glm::vec4(0, 1, 0, 0));
		right = glm::cross(look, up);

		// Determine the camera target point.
		glm::vec3 tgt = position + look;
		// Use the glm::lokat function to calculate the new view matrix using the camera position, target and the up vector.
		V = glm::lookAt(position, tgt, up);
	}

	glm::vec2 FreeCamera::GetRotation() const
	{
		glm::vec2 rotation = glm::vec2(rX, rY);

		return rotation;
	}

	//mouse move handler
	void FreeCamera::OnMouseMove(int button, int x, int y)
	{
		float mouseX = 0, mouseY = 0; //filtered mouse values

		if (button == SDL_BUTTON_LEFT)
		{
			if (state == 1) {
				fov += (y - oldY) / 10.0f;
				SetupProjection(fov, GetAspectRatio());
			}
			else {
				rY += (y - oldY) / 5.0f;
				rX += (oldX - x) / 5.0f;
				mouseX = rX;
				mouseY = rY;
				Rotate(mouseX, mouseY, 0);
			}
			oldX = x;
			oldY = y;
		}
	}

	//mouse click handler
	void FreeCamera::OnMouseDown(int button, int x, int y)
	{
		oldX = x;
		oldY = y;

		if (button == SDL_BUTTON_MIDDLE)
		{
			if (state == 1)
				state = 0;
			else
				state = 1;
		}

	}

	void FreeCamera::Walk(const float dt) {
		translation += (look*speed*dt);
		Update();
	}

	void FreeCamera::Strafe(const float dt) {
		translation += (right*speed*dt);
		Update();
	}

	void FreeCamera::Lift(const float dt) {
		translation += (up*speed*dt);
		Update();
	}

	void FreeCamera::SetTranslation(const glm::vec3& t) {
		translation = t;
		Update();
	}

	glm::vec3 FreeCamera::GetTranslation() const {
		return translation;
	}

	void FreeCamera::SetSpeed(const float s) {
		speed = s;
	}

	const float FreeCamera::GetSpeed() const {
		return speed;
	}

	void FreeCamera::moveCamera(Uint16 key, float dt)
	{
		// Key W
		if (key == 119) {
			Walk(dt*speedC);
		}

		// Key S
		if (key == 115) {
			Walk(-dt*speedC);
		}

		// Key A
		if (key == 97) {
			Strafe(-dt*speedC);
		}

		// Key D
		if (key == 100) {
			Strafe(dt*speedC);
		}

		// Key Q
		if (key == 113) {
			Lift(dt*speedC);
		}

		// Key Z
		if (key == 122) {
			Lift(-dt*speedC);
		}

		// Key ,
		if (key == 44) {
			speedC = 20;
		}

		// Key .
		if (key == 46) {
			speedC = 12;
		}
	}
}