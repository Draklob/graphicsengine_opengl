#version 430 core

// Inputs from the vertex shader.
in vec3 Position;
in vec3 Normal;
in vec2 TexCoord;

//uniform
layout(binding=0) uniform sampler2D textureMap;	//texture map

uniform mat4 MV;				//modelview matrix
uniform vec3 light_position;	//light position in object space
uniform vec3 diffuse_color;		//diffuse colour of surface
uniform vec3 specular_color;	//specular colour of surface
uniform float shininess;		//specular shininess

layout(location = 0) out vec4 vFragColor;	//fragment output colour

//input from the vertex shader
smooth in vec2 vUVout;				//interpolated 2D texture coordinates

//shader constant
const vec3 camPosition = vec3(0,0,0);

void main()
{
	// Multiply the object space light position with the modelview matrix
	//to get the eye space light position
	vec3 spaceLightPosition = ( MV * vec4( light_position, 1 ) ).xyz;

	// Normalize the eye space normal.
	vec3 N = normalize( Normal );

	// Get the light vector and normalize it.
	vec3 L = normalize( spaceLightPosition - Position );

	// Get the view vector and normalize it.
	vec3 V = normalize( camPosition.xyz - Position.xyz );

	// Get the half vector between light and view vector and normalize it.
	vec3 H = normalize( L + V );

	// Calculate the diffuse component.
	float diffuse = max( 0, dot( N, L ) );

	// Calculate the specular component.
	float specular = max( 0, pow( dot( N, H ), shininess ) );

	// Use the interpolated textrue coordinate to lookup the colour from the given texture map.
	vFragColor = texture(textureMap, vUVout.st).rgba;

//	vec4 textureColor = texture(textureMap, vUVout).rgba;
	//vFragColor = diffuse * ( vec4( diffuse_color, 1.0 ) * textureColor ) + specular * vec4( specular_color, 1);
//	vFragColor = diffuse * mix( textureColor, vec4( diffuse_color, 1.0 ), vec4(1) ) + specular * vec4( specular_color, 1);
//	vFragColor = diffuse * textureColor * vec4( diffuse_color, 1.0 ) + specular * vec4( specular_color, 1);
}