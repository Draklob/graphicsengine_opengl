#version 430 core

layout(location=0) in vec3 vVertex;		//per-vertex position  object space position
layout(location=2) in vec3 vNormal;  	//per-vertex normal  Illumination
layout(location=1) in vec2 vUV;			// vertex uv coordinates

// Position output
out vec3 Position;

// Normal output
out vec3 Normal;

// UVs output
out vec2 vUVout;					//2D texture coordinate

//uniform
uniform mat4 MV;
uniform mat3 NM;
uniform mat4 MVP;	//combined modelview projection

void main()
{	
	//multiply the object space normal with the normal matrix 
	//to get the eye space normal
	Normal = normalize( NM * vNormal);

	// Multiply the object space vertex position with the modelview matrix 
	//to get the eye space vertex position
    Position = vec3( MV * vec4(vVertex,1.0) );

	//multiply the combined MVP matrix with the object space position to get the clip space position
	gl_Position = MVP*vec4(vVertex,1.0);

	//get the input vertex x and z value as the 2D texture cooridinate
	vUVout = vUV;
}